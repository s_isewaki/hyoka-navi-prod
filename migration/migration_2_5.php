<?
require_once("common.php");

function do_migration() {
  c2dbBeginTrans();
  $exist = c2dbGetTopRow("SELECT relname FROM pg_class WHERE relkind = 'r' AND relname = 'hnv_system';  ");
  if (count($exist) == 0) {
    $create_sys_sql =
  		" create table hnv_system ( ".
  		" on_maintenance character varying(1), ".
  		" maintenance_message text ".
  		" ); ";
  	c2dbExec($create_sys_sql);
  }

  // 問い合わせ修正対応
  $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = 'hnv_inquiry'::regclass and attname = 'inq_yakusyoku'; ");
  if (count($exist) == 0) {
    c2dbExec(" alter table hnv_inquiry add inq_yakusyoku text ; "); // 役職
  }
  $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = 'hnv_inquiry'::regclass and attname = 'inq_empnm_kana'; ");
  if (count($exist) == 0) {
    c2dbExec(" alter table hnv_inquiry add inq_empnm_kana text ; "); // 氏名かな
  }
  $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = 'hnv_inquiry'::regclass and attname = 'inq_tel'; ");
  if (count($exist) == 0) {
    c2dbExec(" alter table hnv_inquiry add inq_tel text ; "); // 電話
  }
  $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = 'hnv_inquiry'::regclass and attname = 'inq_tel_time'; ");
  if (count($exist) == 0) {
    c2dbExec(" alter table hnv_inquiry add inq_tel_time text ; "); // 電話の繋がる時間
  }

	// FAQ対応
  $exist = c2dbGetTopRow(" SELECT relname FROM pg_class WHERE relkind = 'r' AND relname = 'hnv_faq';  ");
  if (count($exist) == 0) {
    $create_faq_sql =
  		" create table hnv_faq ( ".
  		" faq_seq integer primary key, ".
  		" faq_title text, ".
  		" faq_text text, ".
  		" publish_mode character varying(10), ".
  		" sort_order character varying(10)); ";
  	c2dbExec($create_faq_sql);
  }
  $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = 'hnv_clients'::regclass and attname = 'manual_admin_link'; ");
  if (count($exist) == 0) {
    c2dbExec(" alter table hnv_clients add manual_admin_link text ; "); // 管理者マニュアル
  }
  $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = 'hnv_clients'::regclass and attname = 'manual_hyoka_link'; ");
  if (count($exist) == 0) {
    c2dbExec(" alter table hnv_clients add manual_hyoka_link text ; "); // 評価者マニュアル
  }
  c2dbCommit();


	$clients = c2dbGetRows("select cid from hnv_clients ");
	foreach ($clients as $client) {
    c2dbBeginTrans();
		$cid = $client['cid'];
		// 確定前後の閲覧権限修正
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_revisions'::regclass and attname = 'hyo_disp_uplevel_ongoing'; ");
    if (count($exist) == 0) {
      c2dbExec(" alter table ".$cid."_hnv_revisions add hyo_disp_uplevel_ongoing character varying(100); ");
    }
		// フィードバック修正
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo_feedback'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo_feedback character varying(10); ");
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo_feedback_datetime'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo_feedback_datetime character varying(20); ");
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo_feedback_name'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo_feedback_name text; ");
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo_feedback_remark'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo_feedback_remark text; ");
		// 少数対応
    c2dbExec(" alter table ".$cid."_hnv_casts alter jiko_ttl type real; ");
    c2dbExec(" alter table ".$cid."_hnv_casts alter hyo1_ttl type real; ");
    c2dbExec(" alter table ".$cid."_hnv_casts alter hyo2_ttl type real; ");
    c2dbExec(" alter table ".$cid."_hnv_casts alter hyo3_ttl type real; ");
    c2dbExec(" alter table ".$cid."_hnv_casts alter hyo4_ttl type real; ");
    c2dbExec(" alter table ".$cid."_hnv_casts alter hyo5_ttl type real; ");
    c2dbExec(" alter table ".$cid."_hnv_casts alter hyo6_ttl type real; ");
    c2dbExec(" alter table ".$cid."_hnv_casts alter kakutei_score type real; ");

		// 病院テンプレート対応
    $exist = c2dbGetTopRow(" select relname FROM pg_class WHERE relkind = 'r' AND relname = '".$cid."_hnv_templates';  ");
    if (count($exist) == 0) {
      $create_temp_sql =
  			" create table ".$cid."_hnv_templates ( ".
  			" tmplid integer primary key, ".
  			" filenm text not null default '', ".
  			" tmpl_title text not null default '', ".
  			" file_base64 text, ".
  			" tmpl_info text, ".
  			" publish_mode character varying(10) not null default '', ".
  			" sort_order character varying(10) not null default '', ".
  			" file_md5 character varying(100) ); ";
  		c2dbExec($create_temp_sql);
    }
    c2dbCommit();
	}
	return '正常終了';
}
?>
