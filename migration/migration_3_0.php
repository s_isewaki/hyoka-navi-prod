<?
require_once("common.php");

function do_migration_common() {
  return '対象SQLは存在しません';
}
function do_migration($start,$count) {

  $clients = c2dbGetRows("select cid from hnv_clients order by cid; ");
  for ($i = $start ; $i < ($start+$count) ; $i++) {
  //foreach ($clients as $client) {
    //$cid = $client['cid'];
    $cid = $clients[$i]['cid'];
    c2dbBeginTrans();
    //期間制限
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_revisions'::regclass and attname = 'hyoka_start_ymd'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_revisions add hyoka_start_ymd character varying(10) default '2000/01/01'; ");
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_revisions'::regclass and attname = 'hyoka_end_ymd'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_revisions add hyoka_end_ymd character varying(10) default '9999/12/31'; ");
    //自己評価
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_revisions'::regclass and attname = 'is_jiko_hyoka'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_revisions add is_jiko_hyoka character varying(1) default ''; ");
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_revisions'::regclass and attname = 'is_jiko_sinkoku'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_revisions add is_jiko_sinkoku character varying(1) default ''; ");
    //自己申告（項目別コメント）
    for ($idx=1; $idx<=50; $idx++) {
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'jiko_sinkoku_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add jiko_sinkoku_".$idx." text; ");
    }
    //フリーコメント
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_revisions'::regclass and attname = 'is_jiko_freecomment'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_revisions add is_jiko_freecomment character varying(1) default ''; ");
    $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_revisions'::regclass and attname = 'jiko_freecomment_count'; ");
    if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_revisions add jiko_freecomment_count integer default 1 ; ");
    for ($idx=1; $idx<=8; $idx++) {
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_revisions'::regclass and attname = 'jiko_freecomment_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_revisions add jiko_freecomment_".$idx." text; ");
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'jiko_freecomment_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add jiko_freecomment_".$idx." text; ");
    }

    // 評価表の項目上限UP
    for ($idx=21; $idx<=50; $idx++) {
      //評価表
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_scorebody'::regclass and attname = 'kou_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_scorebody add kou_".$idx." text; ");
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_scorebody'::regclass and attname = 'ten_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_scorebody add ten_".$idx." text; ");
      //職員の評価結果
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'jiko_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add jiko_".$idx." text; ");
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo1_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo1_".$idx." text; ");
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo2_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo2_".$idx." text; ");
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo3_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo3_".$idx." text; ");
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo4_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo4_".$idx." text; ");
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo5_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo5_".$idx." text; ");
      $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = '".$cid."_hnv_casts'::regclass and attname = 'hyo6_".$idx."'; ");
      if (count($exist) == 0) c2dbExec(" alter table ".$cid."_hnv_casts add hyo6_".$idx." text; ");
    }
    c2dbCommit();
  }
  return '正常終了';
}
?>
