<?
require_once("common.php");

function do_migration_common() {
  $exist = c2dbGetTopRow(" select * from pg_attribute where attrelid = 'hnv_clients'::regclass and attname = 'manual_emp_link'; ");
  if (count($exist) == 0) {
    c2dbExec(" alter table hnv_clients add manual_emp_link text ; "); // 管理者マニュアル
  }
  return '正常終了';
}
function do_migration($start,$count) {

  $clients = c2dbGetRows("select cid from hnv_clients order by cid; ");
  for ($i = $start ; $i < ($start+$count) ; $i++) {
    $cid = $clients[$i]['cid'];
    c2dbBeginTrans();
    // 総合判定の期間
    $sql =
      " create table ".$cid."_hnv_revisions_ttl ( ".
      "     revid integer,".
      "     revnm text,".
      "     rev_fr_ymd character varying(10),".
      "     rev_to_ymd character varying(10),".
      "     hyoka_start_ymd character varying(10),".
      "     hyoka_end_ymd character varying(10),".
      "     ttl_rank text,". // ランク基準値
      "     sub_revs text,". // 対象評価期間
      //[type(abo|mbo)]:[revid]:[weight],
      // [weight] ... 0=[weight]&1=[weight]&...
      "     is_active_revision character varying(1) not null default '',".
      "     is_finished character varying(1),". // 確定済みか
      "     constraint ".$cid."_hnv_revisions_ttl_pkey primary key (revid)".
      "); ";

    $exist = c2dbGetTopRow(" select relname from pg_class where relkind = 'r' and relname = '".$cid."_hnv_revisions_ttl';  ");
    if (count($exist) == 0) c2dbExec($sql);

    // 総合判定の評価期間に属する被従業員
    $sql =
      " create table ".$cid."_hnv_casts_ttl ( ".
      "     revid integer not null,".
      "     empid character varying(30) not null,".
      "     empnm character varying(300) not null,".
      "     syozoku text,".
      "     syokusyu text,".
      "     yakusyoku text,".
      "     sisetu text,".
      "     bunrui_ext1 text,".
      "     bunrui_ext2 text,".
      "     toukyu character varying(10),".
      "     is_hyomaster character varying(1),". // 総合判定者なら1
      "     is_excluded character varying(1) not null default '',". // 評価対象外なら1
      "     adjust integer,".
      "     kakutei_score real,". // 総合スコア
      "     kakutei_rank text,". // 総合ランク TODO:本当にここに保存してして常に信じるかは要件次第
      "     sort_order integer,".
      "     constraint ".$cid."_hnv_casts_ttl_pkey primary key (revid, empid)".
      "); ";
    $exist = c2dbGetTopRow(" select relname from pg_class where relkind = 'r' and relname = '".$cid."_hnv_casts_ttl';  ");
    if (count($exist) == 0) c2dbExec($sql);

    // 目標達成度評価の評価期間
    $sql =
      " create table ".$cid."_hnv_revisions_mbo ( ".
      "     revid integer,".
      "     revnm text,".
      "     rev_fr_ymd character varying(10),".
      "     rev_to_ymd character varying(10),".
      "     mokuhyo_start_ymd character varying(10),".
      "     mokuhyo_end_ymd character varying(10),".
      "     hyoka_start_ymd character varying(10),".
      "     hyoka_end_ymd character varying(10),".
      "     setei_step integer not null default 0,".
      "     hyo_level integer not null default 3,". // ここでは段階数を持ち、別テーブルで詳細設定を保持
      "     hyo_step integer not null default 0,".
      "     is_self_hyoka character varying(1),". // 自己評価を行うならなら1
      "     is_active_revision character varying(1) not null default '',".
      "     ttl_rank text,". // ランク基準値
      "     challenge_level integer not null default 3,". // チャレンジ認定の段階数（1~3）
      "     challenge_marks text,". // チャレンジ認定の各段階の記号
      "     challenge_degrees text,". // チャレンジ認定の判断基準
      "     gekihen_level integer not null default 1,". // 激変認定の段階数（1~3）
      "     gekihen_marks text,". // 激変認定の各段階の記号
      "     gekihen_degrees text,". // 激変認定の判断基準
      "     hyo_remark1 text,".
      "     hyo_remark2 text,".
      "     hyo_remark3 text,".
      "     hyo_remark4 text,".
      "     hyo_remark5 text,".
      "     hyo_remark6 text,".
      "     is_finished character varying(1),".
      "     hyo_disp_uplevel character varying(100),".
      "     hyo_disp_uplevel_ongoing character varying(100),".
      "     mokuhyo_max_count integer not null default 20,".
      "     constraint ".$cid."_hnv_revisions_mbo_pkey primary key (revid)".
      "); ";
      $exist = c2dbGetTopRow(" select relname from pg_class where relkind = 'r' and relname = '".$cid."_hnv_revisions_mbo';  ");
      if (count($exist) == 0) c2dbExec($sql);


    // 評価の設定（評価レベル毎）
    $sql =
      " create table ".$cid."_hnv_level_mbo ( ".
      "     revid integer,".
      "     level text,".
      "     score1_1 text,". // チャレンジ認定なし（激変なし）
      "     score1_2 text,". // チャレンジ認定なし（激変2段階目）
      "     score1_3 text,". // チャレンジ認定なし（激変最高評価）
      "     score2_1 text,". // チャレンジ認定２段階目（激変なし）
      "     score2_2 text,". // チャレンジ認定２段階目（激変2段階目）
      "     score2_3 text,". // チャレンジ認定２段階目（激変最高評価）
      "     score3_1 text,". // チャレンジ認定最高評価（激変なし）
      "     score3_2 text,". // チャレンジ認定最高評価（激変2段階目）
      "     score3_3 text,". // チャレンジ認定最高評価（激変最高評価）
      "     degree1 text,".
      "     degree2 text,".
      "     sort_order integer,". // 順番。上から高評価
      "     constraint ".$cid."_hnv_level_mbo_pkey primary key (revid,sort_order)".
      "); ";
    $exist = c2dbGetTopRow(" select relname from pg_class where relkind = 'r' and relname = '".$cid."_hnv_level_mbo';  ");
    if (count($exist) == 0) c2dbExec($sql);

    // 表示項目の設定（項目毎）
    $sql =
      " create table ".$cid."_hnv_form_mbo ( ".
      "     revid integer,".
      "     formid character varying(10),". // 自動的に割り振られる内部コード
      "     type character varying(1),". // 目標は1、評価は2、自己評価は3
      "     label_default text,". // 項目名（初期値）
      "     label text,". // 項目名
      "     is_required character varying(1),". // 必須であれば1。作成時に自動的に割り振られる
      "     is_used character varying(1),". // 使用であれば1。使用されないとユーザーに表示されず、計算に関連する項目も評価されない
      "     form_type character varying(10),". // 項目の種類（text、select、integer等？）
      "     selection text,". // 選択型の時の選択肢（「,」区切り）
      "     max_int_value integer,". // integer型の最大値
      "     min_int_value integer,". // integer型の最小値
      "     max_text_length integer,". // text型の最大文字数
      "     jiko_edit_right character varying(10),". // required、editable、readonly
      "     hyo1_edit_right character varying(10),". // 上記を評価者毎に設定
      "     hyo2_edit_right character varying(10),".
      "     hyo3_edit_right character varying(10),".
      "     hyo4_edit_right character varying(10),".
      "     hyo5_edit_right character varying(10),".
      "     hyo6_edit_right character varying(10),".
      "     sort_order integer,". // 項目表示順
      "     constraint ".$cid."_hnv_form_mbo_pkey primary key (revid , formid)".
      "); ";
    $exist = c2dbGetTopRow(" select relname from pg_class where relkind = 'r' and relname = '".$cid."_hnv_form_mbo';  ");
    if (count($exist) == 0) c2dbExec($sql);

    $fields = array();
    for ($idx=1; $idx<=20; $idx++) $fields[]= " jiko_".$idx." text,";
    for ($idx=1; $idx<=20; $idx++) $fields[]= " hyo1_".$idx." text,";
    for ($idx=1; $idx<=20; $idx++) $fields[]= " hyo2_".$idx." text,";
    for ($idx=1; $idx<=20; $idx++) $fields[]= " hyo3_".$idx." text,";
    for ($idx=1; $idx<=20; $idx++) $fields[]= " hyo4_".$idx." text,";
    for ($idx=1; $idx<=20; $idx++) $fields[]= " hyo5_".$idx." text,";
    for ($idx=1; $idx<=20; $idx++) $fields[]= " hyo6_".$idx." text,";
    // 評価期間に属する被従業員
    $sql =
      " create table ".$cid."_hnv_casts_mbo ( ".
      "     revid integer not null,".
      "     empid character varying(30) not null,".
      "     empnm character varying(300) not null,".
      "     syozoku text,".
      "     syokusyu text,".
      "     yakusyoku text,".
      "     sisetu text,".
      "     bunrui_ext1 text,".
      "     bunrui_ext2 text,".
      "     toukyu character varying(10),".
      "     mokuhyo1_empid character varying(30),".
      "     mokuhyo2_empid character varying(30),".
      "     mokuhyo3_empid character varying(30),".
      "     mokuhyo4_empid character varying(30),".
      "     mokuhyo5_empid character varying(30),".
      "     mokuhyo6_empid character varying(30),".
      "     mokuhyo1_empnm character varying(300),".
      "     mokuhyo2_empnm character varying(300),".
      "     mokuhyo3_empnm character varying(300),".
      "     mokuhyo4_empnm character varying(300),".
      "     mokuhyo5_empnm character varying(300),".
      "     mokuhyo6_empnm character varying(300),".
      "     hyouka1_empid character varying(30),".
      "     hyouka2_empid character varying(30),".
      "     hyouka3_empid character varying(30),".
      "     hyouka4_empid character varying(30),".
      "     hyouka5_empid character varying(30),".
      "     hyouka6_empid character varying(30),".
      "     hyouka1_empnm character varying(300),".
      "     hyouka2_empnm character varying(300),".
      "     hyouka3_empnm character varying(300),".
      "     hyouka4_empnm character varying(300),".
      "     hyouka5_empnm character varying(300),".
      "     hyouka6_empnm character varying(300),".
      "     is_hyomaster character varying(1),". // 最終判定者なら1
      "     kojin_mokuhyo_step integer,". // 目標段階数
      "     kojin_hyo_step integer,". // 評価段階数
      "     is_self_hyoka character varying(1),". // 自己評価を行うならなら1
      "     mokuhyo_count integer default 0,". // 目標項目の数(maxは20)
      "     kakutei_score real,". // 確定評価スコア
      "     ". implode("", $fields). // 評価結果
      "     jiko_ttl real,". // ステップ毎のスコア
      "     hyo1_ttl real,".
      "     hyo2_ttl real,".
      "     hyo3_ttl real,".
      "     hyo4_ttl real,".
      "     hyo5_ttl real,".
      "     hyo6_ttl real,".
      "     hyo9_ttl real,".
      "     hyo1_remark text,".
      "     hyo2_remark text,".
      "     hyo3_remark text,".
      "     hyo4_remark text,".
      "     hyo5_remark text,".
      "     hyo6_remark text,".
      "     adjust integer,". // 調整スコア
      "     progress_mokuhyo integer not null default 0,". // 目標設定の進捗
      "     progress_hyoka integer not null default 1,". // 評価の進捗
      "     status character varying(10) default 'start',". // 全体の進捗状況（start>mokuhyo>approved>hyoka>(kakutei)）
      "     is_excluded character varying(1) not null default '',". // 評価対象外なら1
      "     remark text,".
      "     sort_order integer,".
      "     hyo_feedback character varying(10),". // フィードバック関連
      "     hyo_feedback_datetime character varying(20),".
      "     hyo_feedback_name text,".
      "     hyo_feedback_remark text,".
      "     constraint ".$cid."_hnv_casts_mbo_pkey primary key (revid, empid)".
      "); ";
    $exist = c2dbGetTopRow(" select relname from pg_class where relkind = 'r' and relname = '".$cid."_hnv_casts_mbo';  ");
    if (count($exist) == 0) c2dbExec($sql);

    c2dbExec(" alter table ".$cid."_hnv_casts alter hyo9_ttl type real using CAST(hyo9_ttl AS float) ; ");

    // 目標シートの内容＆結果（個人別×項目別）
    $sql =
      " create table ".$cid."_hnv_sheet_mbo ( ".
      "     revid integer not null,".
      "     empid character varying(30) not null,".
      "     formid character varying(10),". // 自動的に割り振られる内部コード（フォームへの参照）
      "     item_index integer,". // 項目ID=並び順（1スタート）
      "     value text,". // TODO:全てtextに入れてしまってよいか。検証しつつ決める
      "     constraint ".$cid."_hnv_sheet_mbo_pkey primary key (revid, empid,formid,item_index)".
      "); ";
    $exist = c2dbGetTopRow(" select relname from pg_class where relkind = 'r' and relname = '".$cid."_hnv_sheet_mbo';  ");
    if (count($exist) == 0) c2dbExec($sql);

    $revisionRows = c2dbGetRows(" select * from ".$cid."_hnv_revisions ");
    foreach ($revisionRows as $revisionRow) {
      $sql =
      "update ".$cid."_hnv_casts set".
      " hyo9_ttl = coalesce(cast(adjust as float), 0) + coalesce(cast(hyo".$revisionRow["hyo_step"]."_ttl as float), 0)".
      " where revid = ".$revisionRow['revid']." and (kojin_hyo_step is null or kojin_hyo_step = 0) ";
      c2dbExec($sql);
      for ($idx = 1 ; $idx <= 6 ; $idx++) {
        $sql =
        "update ".$cid."_hnv_casts set hyo9_ttl = coalesce(cast(adjust as float), 0) + coalesce(cast(hyo".$idx."_ttl as float), 0)".
        " where revid = ".$revisionRow['revid']." and kojin_hyo_step = ".$idx." ";
        c2dbExec($sql);
      }
    }
    c2dbCommit();
  }
  return '正常終了';
}
?>
