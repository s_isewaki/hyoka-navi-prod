NK福岡よりの依頼「人事評価Navigator」というサイトを、
trial.comedix.jpに同居構築します。


ドメインはhyoka-navi.jpとします。（ドメイン取得はこれからです）

これは、サンプルサイトではありません。

comedixトライアルサイトが同居していますが、
メンテのためのapacheやpostgresqlのサービス停止や再起動は、幾分ご注意いただくことになります。




ついては、trial.comedix.jpサーバのhttpd.confに、
以下を追記しましたので報告いたします。
----------------------------------------------------------------------

NameVirtualHost *:80


<VirtualHost *:80>
    ServerName trial.comedix.jp
    DocumentRoot /var/www/html
    UseCanonicalName off
</VirtualHost>

<VirtualHost *:80>
    ServerName hyoka-navi.jp
    DocumentRoot /var/www/hyoka-navi.jp
    UseCanonicalName off
    CustomLog logs/hyoka-navi_access_log combined
    ErrorLog logs/hyoka-navi_error_log
</VirtualHost>

<Directory "/var/www/hyoka-navi.jp">
    AllowOverride All
</Directory>



また、コンテンツルートは以下です。
----------------------------------------------------------------------
/var/www/hyoka-navi.jp


および、ローカルDBに、以下を追加しました。
----------------------------------------------------------------------
DB名：hyoka_navi
エンコーディング：UTF8


定期バックアップ（３時間ごと）
----------------------------------------------------------------------
#hyoka_navi db backup (per 3 hours)
0 */3 * * * su - postgres -c "pg_dump -f /var/www/hyoka-navi.jp/dev/db/hyoka_navi.dump -F c hyoka_navi"