--
-- PostgreSQL database dump
--

-- Started on 2015-06-09 08:27:31

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- TOC entry 2131 (class 1262 OID 53902107)
-- Name: hyoka_navi; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE hyoka_navi WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';


ALTER DATABASE hyoka_navi OWNER TO postgres;

\connect hyoka_navi

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = true;

--
-- TOC entry 1629 (class 1259 OID 53902108)
-- Dependencies: 1954 1955 1956 1957 1958 1959 1960 1961 1962 1963 1964 6
-- Name: byouin2_nkjk_emps; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE byouin2_nkjk_emps (
    revid integer NOT NULL,
    empnm character varying(200) NOT NULL,
    emp2nm character varying(200) NOT NULL,
    emp_personal_id character varying(12),
    syozoku text,
    syokusyu text,
    yakusyoku text,
    kihonkyu integer DEFAULT 0 NOT NULL,
    toukyu character varying(10),
    hyoid integer DEFAULT 0 NOT NULL,
    hyouka1_empnm character varying(200),
    hyouka2_empnm character varying(200),
    hyouka3_empnm character varying(200),
    hyouka4_empnm character varying(200),
    hyouka5_empnm character varying(200),
    hyouka6_empnm character varying(200),
    hyouka1_emp2nm character varying(200),
    hyouka2_emp2nm character varying(200),
    hyouka3_emp2nm character varying(200),
    hyouka4_emp2nm character varying(200),
    hyouka5_emp2nm character varying(200),
    hyouka6_emp2nm character varying(200),
    is_hyomaster character varying(1),
    jiko_1 text,
    jiko_2 text,
    jiko_3 text,
    jiko_4 text,
    jiko_5 text,
    jiko_6 text,
    jiko_7 text,
    jiko_8 text,
    jiko_9 text,
    jiko_10 text,
    jiko_11 text,
    jiko_12 text,
    jiko_13 text,
    jiko_14 text,
    jiko_15 text,
    jiko_16 text,
    jiko_17 text,
    jiko_18 text,
    jiko_19 text,
    jiko_20 text,
    hyo1_1 text,
    hyo1_2 text,
    hyo1_3 text,
    hyo1_4 text,
    hyo1_5 text,
    hyo1_6 text,
    hyo1_7 text,
    hyo1_8 text,
    hyo1_9 text,
    hyo1_10 text,
    hyo1_11 text,
    hyo1_12 text,
    hyo1_13 text,
    hyo1_14 text,
    hyo1_15 text,
    hyo1_16 text,
    hyo1_17 text,
    hyo1_18 text,
    hyo1_19 text,
    hyo1_20 text,
    hyo2_1 text,
    hyo2_2 text,
    hyo2_3 text,
    hyo2_4 text,
    hyo2_5 text,
    hyo2_6 text,
    hyo2_7 text,
    hyo2_8 text,
    hyo2_9 text,
    hyo2_10 text,
    hyo2_11 text,
    hyo2_12 text,
    hyo2_13 text,
    hyo2_14 text,
    hyo2_15 text,
    hyo2_16 text,
    hyo2_17 text,
    hyo2_18 text,
    hyo2_19 text,
    hyo2_20 text,
    hyo3_1 text,
    hyo3_2 text,
    hyo3_3 text,
    hyo3_4 text,
    hyo3_5 text,
    hyo3_6 text,
    hyo3_7 text,
    hyo3_8 text,
    hyo3_9 text,
    hyo3_10 text,
    hyo3_11 text,
    hyo3_12 text,
    hyo3_13 text,
    hyo3_14 text,
    hyo3_15 text,
    hyo3_16 text,
    hyo3_17 text,
    hyo3_18 text,
    hyo3_19 text,
    hyo3_20 text,
    hyo4_1 text,
    hyo4_2 text,
    hyo4_3 text,
    hyo4_4 text,
    hyo4_5 text,
    hyo4_6 text,
    hyo4_7 text,
    hyo4_8 text,
    hyo4_9 text,
    hyo4_10 text,
    hyo4_11 text,
    hyo4_12 text,
    hyo4_13 text,
    hyo4_14 text,
    hyo4_15 text,
    hyo4_16 text,
    hyo4_17 text,
    hyo4_18 text,
    hyo4_19 text,
    hyo4_20 text,
    hyo5_1 text,
    hyo5_2 text,
    hyo5_3 text,
    hyo5_4 text,
    hyo5_5 text,
    hyo5_6 text,
    hyo5_7 text,
    hyo5_8 text,
    hyo5_9 text,
    hyo5_10 text,
    hyo5_11 text,
    hyo5_12 text,
    hyo5_13 text,
    hyo5_14 text,
    hyo5_15 text,
    hyo5_16 text,
    hyo5_17 text,
    hyo5_18 text,
    hyo5_19 text,
    hyo5_20 text,
    hyo6_1 text,
    hyo6_2 text,
    hyo6_3 text,
    hyo6_4 text,
    hyo6_5 text,
    hyo6_6 text,
    hyo6_7 text,
    hyo6_8 text,
    hyo6_9 text,
    hyo6_10 text,
    hyo6_11 text,
    hyo6_12 text,
    hyo6_13 text,
    hyo6_14 text,
    hyo6_15 text,
    hyo6_16 text,
    hyo6_17 text,
    hyo6_18 text,
    hyo6_19 text,
    hyo6_20 text,
    jiko_ttl integer DEFAULT 0 NOT NULL,
    hyo1_ttl integer DEFAULT 0 NOT NULL,
    hyo2_ttl integer DEFAULT 0 NOT NULL,
    hyo3_ttl integer DEFAULT 0 NOT NULL,
    hyo4_ttl integer DEFAULT 0 NOT NULL,
    hyo5_ttl integer DEFAULT 0 NOT NULL,
    hyo6_ttl integer DEFAULT 0 NOT NULL,
    progress integer DEFAULT 0 NOT NULL,
    is_excluded character varying(1) DEFAULT ''::character varying NOT NULL,
    remark text,
    hyo1_remark text,
    hyo2_remark text,
    hyo3_remark text,
    hyo4_remark text,
    hyo5_remark text,
    hyo6_remark text,
    hyo9_ttl text,
    adjust integer,
    kojin_hyo_step integer,
    kakutei_score integer,
    kakutei_rank integer
);


ALTER TABLE public.byouin2_nkjk_emps OWNER TO postgres;

--
-- TOC entry 1630 (class 1259 OID 53902125)
-- Dependencies: 6
-- Name: byouin2_nkjk_hyobody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE byouin2_nkjk_hyobody (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    kouid integer NOT NULL,
    kou_title text,
    kou_teigi text,
    kou_1 text,
    kou_2 text,
    kou_3 text,
    kou_4 text,
    kou_5 text,
    kou_6 text,
    kou_7 text,
    kou_8 text,
    kou_9 text,
    kou_10 text,
    kou_11 text,
    kou_12 text,
    kou_13 text,
    kou_14 text,
    kou_15 text,
    kou_16 text,
    kou_17 text,
    kou_18 text,
    kou_19 text,
    kou_20 text,
    ten_1 text,
    ten_2 text,
    ten_3 text,
    ten_4 text,
    ten_5 text,
    ten_6 text,
    ten_7 text,
    ten_8 text,
    ten_9 text,
    ten_10 text,
    ten_11 text,
    ten_12 text,
    ten_13 text,
    ten_14 text,
    ten_15 text,
    ten_16 text,
    ten_17 text,
    ten_18 text,
    ten_19 text,
    ten_20 text
);


ALTER TABLE public.byouin2_nkjk_hyobody OWNER TO postgres;

--
-- TOC entry 1631 (class 1259 OID 53902131)
-- Dependencies: 1965 6
-- Name: byouin2_nkjk_hyohead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE byouin2_nkjk_hyohead (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    hyonm text,
    setumonsu integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.byouin2_nkjk_hyohead OWNER TO postgres;

--
-- TOC entry 1632 (class 1259 OID 53902138)
-- Dependencies: 1966 1967 6
-- Name: byouin2_nkjk_revisions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE byouin2_nkjk_revisions (
    revid integer NOT NULL,
    revnm text,
    rev_fr_ymd character varying(10),
    rev_to_ymd character varying(10),
    hyo_level text,
    ttl_rank text,
    hyo_step integer DEFAULT 0 NOT NULL,
    hyo_listinput text,
    is_active_revision character varying(1) DEFAULT ''::character varying NOT NULL,
    hyo_remark1 text,
    hyo_remark2 text,
    hyo_remark3 text,
    hyo_remark4 text,
    hyo_remark5 text,
    hyo_remark6 text,
    is_finished character varying(1)
);


ALTER TABLE public.byouin2_nkjk_revisions OWNER TO postgres;

SET default_with_oids = false;

--
-- TOC entry 1641 (class 1259 OID 53902221)
-- Dependencies: 1977 6
-- Name: hnv_clients; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hnv_clients (
    cid character varying(20) NOT NULL,
    cnm text,
    is_active character varying(1) DEFAULT ''::character varying NOT NULL,
    max_revid integer,
    info_toppage1 text
);


ALTER TABLE public.hnv_clients OWNER TO postgres;

--
-- TOC entry 2134 (class 0 OID 0)
-- Dependencies: 1641
-- Name: TABLE hnv_clients; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE hnv_clients IS '顧客テーブル（ or 利用団体テーブル or 病院マスタ ） 病院など利用機関単位で作成';


--
-- TOC entry 2135 (class 0 OID 0)
-- Dependencies: 1641
-- Name: COLUMN hnv_clients.cid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN hnv_clients.cid IS '顧客ID。半角英数小文字限定。アンダースコアも禁止。先頭はアルファベットとすること。ログインIDの接頭語になる。また、DBテーブルの顧客エリアとして、動的作成されるテーブル名プレフィクスとして利用される。';


--
-- TOC entry 2136 (class 0 OID 0)
-- Dependencies: 1641
-- Name: COLUMN hnv_clients.max_revid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN hnv_clients.max_revid IS '現在の最大リビジョンID数値。リビジョンを作成するごとに１増やす。顧客を最初に作成した段階ではゼロ。リビジョンのデリートを行ってもリビジョンは欠番させないため、この値は増え続ける。シーケンスの代わり。';


--
-- TOC entry 1642 (class 1259 OID 53902228)
-- Dependencies: 1978 6
-- Name: hnv_login; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hnv_login (
    cid character varying(20) NOT NULL,
    login_id character varying(100) NOT NULL,
    login_pw character varying(100),
    login_empnm text,
    is_admin character varying(1),
    login_emp2nm character varying(200),
    is_active character varying(1) DEFAULT '1'::character varying NOT NULL
);


ALTER TABLE public.hnv_login OWNER TO postgres;

--
-- TOC entry 2137 (class 0 OID 0)
-- Dependencies: 1642
-- Name: COLUMN hnv_login.cid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN hnv_login.cid IS '顧客ID。ログインIDの部分としても利用されるため、半角英数小文字推奨';


--
-- TOC entry 2138 (class 0 OID 0)
-- Dependencies: 1642
-- Name: COLUMN hnv_login.login_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN hnv_login.login_id IS 'ログイン用のID。半角英数小文字推奨。';


--
-- TOC entry 2139 (class 0 OID 0)
-- Dependencies: 1642
-- Name: COLUMN hnv_login.login_pw; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN hnv_login.login_pw IS 'ログインパスワード';


--
-- TOC entry 1643 (class 1259 OID 53902235)
-- Dependencies: 1979 1980 1981 1982 6
-- Name: hnv_templates; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hnv_templates (
    tmplid integer NOT NULL,
    filenm text DEFAULT ''::text NOT NULL,
    tmpl_title text DEFAULT ''::text NOT NULL,
    file_base64 text,
    tmpl_info text,
    publish_mode character varying(10) DEFAULT ''::character varying NOT NULL,
    sort_order character varying(10) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.hnv_templates OWNER TO postgres;

--
-- TOC entry 1644 (class 1259 OID 53902245)
-- Dependencies: 6
-- Name: hnv_upload_csv; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE hnv_upload_csv (
    upload_seq integer NOT NULL,
    upload_ymdhms character varying(14),
    upload_empid character varying(30),
    upload_empnm character varying(300),
    cid character varying(20),
    revid integer,
    csv_rawdata text,
    upd_or_ins character varying(20),
    upload_error text
);


ALTER TABLE public.hnv_upload_csv OWNER TO postgres;

--
-- TOC entry 1645 (class 1259 OID 53902251)
-- Dependencies: 6
-- Name: hnv_upload_csv_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hnv_upload_csv_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.hnv_upload_csv_seq OWNER TO postgres;

SET default_with_oids = true;

--
-- TOC entry 1633 (class 1259 OID 53902146)
-- Dependencies: 1968 1969 1970 1971 6
-- Name: mss_nkjk_emps; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE mss_nkjk_emps (
    revid integer NOT NULL,
    empnm character varying(200) NOT NULL,
    emp2nm character varying(200) NOT NULL,
    emp_personal_id character varying(200),
    syozoku text,
    syokusyu text,
    yakusyoku text,
    kihonkyu integer DEFAULT 0 NOT NULL,
    toukyu character varying(10),
    hyoid integer DEFAULT 0 NOT NULL,
    hyouka1_empid character varying(30),
    hyouka2_empnm character varying(200),
    hyouka3_empnm character varying(200),
    hyouka4_empnm character varying(200),
    hyouka5_empnm character varying(200),
    hyouka6_empnm character varying(200),
    hyouka1_emp2nm character varying(200),
    hyouka2_emp2nm character varying(200),
    hyouka3_emp2nm character varying(200),
    hyouka4_emp2nm character varying(200),
    hyouka5_emp2nm character varying(200),
    hyouka6_emp2nm character varying(200),
    is_hyomaster character varying(1),
    jiko_1 text,
    jiko_2 text,
    jiko_3 text,
    jiko_4 text,
    jiko_5 text,
    jiko_6 text,
    jiko_7 text,
    jiko_8 text,
    jiko_9 text,
    jiko_10 text,
    jiko_11 text,
    jiko_12 text,
    jiko_13 text,
    jiko_14 text,
    jiko_15 text,
    jiko_16 text,
    jiko_17 text,
    jiko_18 text,
    jiko_19 text,
    jiko_20 text,
    hyo1_1 text,
    hyo1_2 text,
    hyo1_3 text,
    hyo1_4 text,
    hyo1_5 text,
    hyo1_6 text,
    hyo1_7 text,
    hyo1_8 text,
    hyo1_9 text,
    hyo1_10 text,
    hyo1_11 text,
    hyo1_12 text,
    hyo1_13 text,
    hyo1_14 text,
    hyo1_15 text,
    hyo1_16 text,
    hyo1_17 text,
    hyo1_18 text,
    hyo1_19 text,
    hyo1_20 text,
    hyo2_1 text,
    hyo2_2 text,
    hyo2_3 text,
    hyo2_4 text,
    hyo2_5 text,
    hyo2_6 text,
    hyo2_7 text,
    hyo2_8 text,
    hyo2_9 text,
    hyo2_10 text,
    hyo2_11 text,
    hyo2_12 text,
    hyo2_13 text,
    hyo2_14 text,
    hyo2_15 text,
    hyo2_16 text,
    hyo2_17 text,
    hyo2_18 text,
    hyo2_19 text,
    hyo2_20 text,
    hyo3_1 text,
    hyo3_2 text,
    hyo3_3 text,
    hyo3_4 text,
    hyo3_5 text,
    hyo3_6 text,
    hyo3_7 text,
    hyo3_8 text,
    hyo3_9 text,
    hyo3_10 text,
    hyo3_11 text,
    hyo3_12 text,
    hyo3_13 text,
    hyo3_14 text,
    hyo3_15 text,
    hyo3_16 text,
    hyo3_17 text,
    hyo3_18 text,
    hyo3_19 text,
    hyo3_20 text,
    hyo4_1 text,
    hyo4_2 text,
    hyo4_3 text,
    hyo4_4 text,
    hyo4_5 text,
    hyo4_6 text,
    hyo4_7 text,
    hyo4_8 text,
    hyo4_9 text,
    hyo4_10 text,
    hyo4_11 text,
    hyo4_12 text,
    hyo4_13 text,
    hyo4_14 text,
    hyo4_15 text,
    hyo4_16 text,
    hyo4_17 text,
    hyo4_18 text,
    hyo4_19 text,
    hyo4_20 text,
    hyo5_1 text,
    hyo5_2 text,
    hyo5_3 text,
    hyo5_4 text,
    hyo5_5 text,
    hyo5_6 text,
    hyo5_7 text,
    hyo5_8 text,
    hyo5_9 text,
    hyo5_10 text,
    hyo5_11 text,
    hyo5_12 text,
    hyo5_13 text,
    hyo5_14 text,
    hyo5_15 text,
    hyo5_16 text,
    hyo5_17 text,
    hyo5_18 text,
    hyo5_19 text,
    hyo5_20 text,
    hyo6_1 text,
    hyo6_2 text,
    hyo6_3 text,
    hyo6_4 text,
    hyo6_5 text,
    hyo6_6 text,
    hyo6_7 text,
    hyo6_8 text,
    hyo6_9 text,
    hyo6_10 text,
    hyo6_11 text,
    hyo6_12 text,
    hyo6_13 text,
    hyo6_14 text,
    hyo6_15 text,
    hyo6_16 text,
    hyo6_17 text,
    hyo6_18 text,
    hyo6_19 text,
    hyo6_20 text,
    jiko_ttl integer,
    hyo1_ttl integer,
    hyo2_ttl integer,
    hyo3_ttl integer,
    hyo4_ttl integer,
    hyo5_ttl integer,
    hyo6_ttl integer,
    hyo9_ttl integer,
    adjust integer,
    progress integer DEFAULT 0 NOT NULL,
    is_excluded character varying(1) DEFAULT ''::character varying NOT NULL,
    remark text,
    kojin_hyo_step integer,
    kakutei_score integer,
    kakutei_rank integer
);


ALTER TABLE public.mss_nkjk_emps OWNER TO postgres;

--
-- TOC entry 1634 (class 1259 OID 53902156)
-- Dependencies: 6
-- Name: mss_nkjk_hyobody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE mss_nkjk_hyobody (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    kouid integer NOT NULL,
    kou_title text,
    kou_teigi text,
    kou_1 text,
    kou_2 text,
    kou_3 text,
    kou_4 text,
    kou_5 text,
    kou_6 text,
    kou_7 text,
    kou_8 text,
    kou_9 text,
    kou_10 text,
    kou_11 text,
    kou_12 text,
    kou_13 text,
    kou_14 text,
    kou_15 text,
    kou_16 text,
    kou_17 text,
    kou_18 text,
    kou_19 text,
    kou_20 text,
    ten_1 text,
    ten_2 text,
    ten_3 text,
    ten_4 text,
    ten_5 text,
    ten_6 text,
    ten_7 text,
    ten_8 text,
    ten_9 text,
    ten_10 text,
    ten_11 text,
    ten_12 text,
    ten_13 text,
    ten_14 text,
    ten_15 text,
    ten_16 text,
    ten_17 text,
    ten_18 text,
    ten_19 text,
    ten_20 text
);


ALTER TABLE public.mss_nkjk_hyobody OWNER TO postgres;

--
-- TOC entry 1635 (class 1259 OID 53902162)
-- Dependencies: 1972 6
-- Name: mss_nkjk_hyohead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE mss_nkjk_hyohead (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    hyonm text,
    setumonsu integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.mss_nkjk_hyohead OWNER TO postgres;

--
-- TOC entry 1636 (class 1259 OID 53902178)
-- Dependencies: 1973 1974 6
-- Name: mss_nkjk_revisions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE mss_nkjk_revisions (
    revid integer NOT NULL,
    revnm text,
    rev_fr_ymd character varying(10),
    rev_to_ymd character varying(10),
    hyo_level text,
    ttl_rank text,
    hyo_step integer DEFAULT 0 NOT NULL,
    hyo_listinput text,
    is_active_revision character varying(1) DEFAULT ''::character varying NOT NULL,
    hyo_remark1 text,
    hyo_remark2 text,
    hyo_remark3 text,
    hyo_remark4 text,
    hyo_remark5 text,
    hyo_remark6 text,
    is_finished character varying(1)
);


ALTER TABLE public.mss_nkjk_revisions OWNER TO postgres;

--
-- TOC entry 1637 (class 1259 OID 53902186)
-- Dependencies: 1975 6
-- Name: nk_hnv_casts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nk_hnv_casts (
    revid integer NOT NULL,
    empid character varying(30) NOT NULL,
    syozoku text,
    syokusyu text,
    yakusyoku text,
    kihonkyu integer,
    toukyu character varying(10),
    hyoid integer,
    is_hyomaster character varying(1),
    jiko_1 text,
    jiko_2 text,
    jiko_3 text,
    jiko_4 text,
    jiko_5 text,
    jiko_6 text,
    jiko_7 text,
    jiko_8 text,
    jiko_9 text,
    jiko_10 text,
    jiko_11 text,
    jiko_12 text,
    jiko_13 text,
    jiko_14 text,
    jiko_15 text,
    jiko_16 text,
    jiko_17 text,
    jiko_18 text,
    jiko_19 text,
    jiko_20 text,
    hyo1_1 text,
    hyo1_2 text,
    hyo1_3 text,
    hyo1_4 text,
    hyo1_5 text,
    hyo1_6 text,
    hyo1_7 text,
    hyo1_8 text,
    hyo1_9 text,
    hyo1_10 text,
    hyo1_11 text,
    hyo1_12 text,
    hyo1_13 text,
    hyo1_14 text,
    hyo1_15 text,
    hyo1_16 text,
    hyo1_17 text,
    hyo1_18 text,
    hyo1_19 text,
    hyo1_20 text,
    hyo2_1 text,
    hyo2_2 text,
    hyo2_3 text,
    hyo2_4 text,
    hyo2_5 text,
    hyo2_6 text,
    hyo2_7 text,
    hyo2_8 text,
    hyo2_9 text,
    hyo2_10 text,
    hyo2_11 text,
    hyo2_12 text,
    hyo2_13 text,
    hyo2_14 text,
    hyo2_15 text,
    hyo2_16 text,
    hyo2_17 text,
    hyo2_18 text,
    hyo2_19 text,
    hyo2_20 text,
    hyo3_1 text,
    hyo3_2 text,
    hyo3_3 text,
    hyo3_4 text,
    hyo3_5 text,
    hyo3_6 text,
    hyo3_7 text,
    hyo3_8 text,
    hyo3_9 text,
    hyo3_10 text,
    hyo3_11 text,
    hyo3_12 text,
    hyo3_13 text,
    hyo3_14 text,
    hyo3_15 text,
    hyo3_16 text,
    hyo3_17 text,
    hyo3_18 text,
    hyo3_19 text,
    hyo3_20 text,
    hyo4_1 text,
    hyo4_2 text,
    hyo4_3 text,
    hyo4_4 text,
    hyo4_5 text,
    hyo4_6 text,
    hyo4_7 text,
    hyo4_8 text,
    hyo4_9 text,
    hyo4_10 text,
    hyo4_11 text,
    hyo4_12 text,
    hyo4_13 text,
    hyo4_14 text,
    hyo4_15 text,
    hyo4_16 text,
    hyo4_17 text,
    hyo4_18 text,
    hyo4_19 text,
    hyo4_20 text,
    hyo5_1 text,
    hyo5_2 text,
    hyo5_3 text,
    hyo5_4 text,
    hyo5_5 text,
    hyo5_6 text,
    hyo5_7 text,
    hyo5_8 text,
    hyo5_9 text,
    hyo5_10 text,
    hyo5_11 text,
    hyo5_12 text,
    hyo5_13 text,
    hyo5_14 text,
    hyo5_15 text,
    hyo5_16 text,
    hyo5_17 text,
    hyo5_18 text,
    hyo5_19 text,
    hyo5_20 text,
    hyo6_1 text,
    hyo6_2 text,
    hyo6_3 text,
    hyo6_4 text,
    hyo6_5 text,
    hyo6_6 text,
    hyo6_7 text,
    hyo6_8 text,
    hyo6_9 text,
    hyo6_10 text,
    hyo6_11 text,
    hyo6_12 text,
    hyo6_13 text,
    hyo6_14 text,
    hyo6_15 text,
    hyo6_16 text,
    hyo6_17 text,
    hyo6_18 text,
    hyo6_19 text,
    hyo6_20 text,
    jiko_ttl integer,
    hyo1_ttl integer,
    hyo2_ttl integer,
    hyo3_ttl integer,
    hyo4_ttl integer,
    hyo5_ttl integer,
    hyo6_ttl integer,
    progress integer,
    is_excluded character varying(1) DEFAULT ''::character varying NOT NULL,
    remark text,
    hyouka1_empid character varying(30),
    hyouka2_empid character varying(30),
    hyouka3_empid character varying(30),
    hyouka4_empid character varying(30),
    hyouka5_empid character varying(30),
    hyouka6_empid character varying(30),
    hyo1_remark text,
    hyo2_remark text,
    hyo3_remark text,
    hyo4_remark text,
    hyo5_remark text,
    hyo6_remark text,
    hyo9_ttl text,
    adjust integer,
    kojin_hyo_step integer,
    kakutei_score integer,
    kakutei_rank integer,
    empnm character varying(300),
    hyouka1_empnm character varying(300),
    hyouka2_empnm character varying(300),
    hyouka3_empnm character varying(300),
    hyouka4_empnm character varying(300),
    hyouka5_empnm character varying(300),
    hyouka6_empnm character varying(300)
);


ALTER TABLE public.nk_hnv_casts OWNER TO postgres;

SET default_with_oids = false;

--
-- TOC entry 1660 (class 1259 OID 54816108)
-- Dependencies: 2011 6
-- Name: nk_hnv_emps; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nk_hnv_emps (
    empid character varying(30) NOT NULL,
    login_id character varying(100) NOT NULL,
    login_pw character varying(100),
    empnm text,
    is_admin character varying(1),
    is_active character varying(1) DEFAULT '1'::character varying NOT NULL
);


ALTER TABLE public.nk_hnv_emps OWNER TO postgres;

--
-- TOC entry 2140 (class 0 OID 0)
-- Dependencies: 1660
-- Name: COLUMN nk_hnv_emps.empid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nk_hnv_emps.empid IS '職員ID';


--
-- TOC entry 2141 (class 0 OID 0)
-- Dependencies: 1660
-- Name: COLUMN nk_hnv_emps.login_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nk_hnv_emps.login_id IS 'ログイン用のID。半角英数小文字推奨。';


--
-- TOC entry 2142 (class 0 OID 0)
-- Dependencies: 1660
-- Name: COLUMN nk_hnv_emps.login_pw; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN nk_hnv_emps.login_pw IS 'ログインパスワード';


SET default_with_oids = true;

--
-- TOC entry 1640 (class 1259 OID 53902214)
-- Dependencies: 1976 6
-- Name: nk_hnv_revisions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nk_hnv_revisions (
    revid integer NOT NULL,
    revnm text,
    rev_fr_ymd character varying(10),
    rev_to_ymd character varying(10),
    hyo_level text,
    ttl_rank text,
    hyo_step integer,
    hyo_listinput text,
    is_active_revision character varying(1) DEFAULT ''::character varying NOT NULL,
    hyo_remark1 text,
    hyo_remark2 text,
    hyo_remark3 text,
    hyo_remark4 text,
    hyo_remark5 text,
    hyo_remark6 text,
    is_finished character varying(1)
);


ALTER TABLE public.nk_hnv_revisions OWNER TO postgres;

--
-- TOC entry 1638 (class 1259 OID 53902193)
-- Dependencies: 6
-- Name: nk_hnv_scorebody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nk_hnv_scorebody (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    kouid integer NOT NULL,
    kou_title text,
    kou_teigi text,
    kou_1 text,
    kou_2 text,
    kou_3 text,
    kou_4 text,
    kou_5 text,
    kou_6 text,
    kou_7 text,
    kou_8 text,
    kou_9 text,
    kou_10 text,
    kou_11 text,
    kou_12 text,
    kou_13 text,
    kou_14 text,
    kou_15 text,
    kou_16 text,
    kou_17 text,
    kou_18 text,
    kou_19 text,
    kou_20 text,
    ten_1 text,
    ten_2 text,
    ten_3 text,
    ten_4 text,
    ten_5 text,
    ten_6 text,
    ten_7 text,
    ten_8 text,
    ten_9 text,
    ten_10 text,
    ten_11 text,
    ten_12 text,
    ten_13 text,
    ten_14 text,
    ten_15 text,
    ten_16 text,
    ten_17 text,
    ten_18 text,
    ten_19 text,
    ten_20 text
);


ALTER TABLE public.nk_hnv_scorebody OWNER TO postgres;

--
-- TOC entry 1639 (class 1259 OID 53902199)
-- Dependencies: 6
-- Name: nk_hnv_scorehead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nk_hnv_scorehead (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    hyonm text,
    setumonsu integer
);


ALTER TABLE public.nk_hnv_scorehead OWNER TO postgres;

--
-- TOC entry 1664 (class 1259 OID 54869086)
-- Dependencies: 2014 2015 2016 2017 6
-- Name: phase1a_hnv_casts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE phase1a_hnv_casts (
    revid integer NOT NULL,
    empid character varying(30) NOT NULL,
    empnm character varying(300) NOT NULL,
    syozoku text,
    syokusyu text,
    yakusyoku text,
    kihonkyu integer DEFAULT 0 NOT NULL,
    toukyu character varying(10),
    hyoid integer DEFAULT 0 NOT NULL,
    hyouka1_empid character varying(30),
    hyouka2_empid character varying(30),
    hyouka3_empid character varying(30),
    hyouka4_empid character varying(30),
    hyouka5_empid character varying(30),
    hyouka6_empid character varying(30),
    hyouka1_empnm character varying(300),
    hyouka2_empnm character varying(300),
    hyouka3_empnm character varying(300),
    hyouka4_empnm character varying(300),
    hyouka5_empnm character varying(300),
    hyouka6_empnm character varying(300),
    is_hyomaster character varying(1),
    kojin_hyo_step integer,
    jiko_1 text,
    jiko_2 text,
    jiko_3 text,
    jiko_4 text,
    jiko_5 text,
    jiko_6 text,
    jiko_7 text,
    jiko_8 text,
    jiko_9 text,
    jiko_10 text,
    jiko_11 text,
    jiko_12 text,
    jiko_13 text,
    jiko_14 text,
    jiko_15 text,
    jiko_16 text,
    jiko_17 text,
    jiko_18 text,
    jiko_19 text,
    jiko_20 text,
    hyo1_1 text,
    hyo1_2 text,
    hyo1_3 text,
    hyo1_4 text,
    hyo1_5 text,
    hyo1_6 text,
    hyo1_7 text,
    hyo1_8 text,
    hyo1_9 text,
    hyo1_10 text,
    hyo1_11 text,
    hyo1_12 text,
    hyo1_13 text,
    hyo1_14 text,
    hyo1_15 text,
    hyo1_16 text,
    hyo1_17 text,
    hyo1_18 text,
    hyo1_19 text,
    hyo1_20 text,
    hyo2_1 text,
    hyo2_2 text,
    hyo2_3 text,
    hyo2_4 text,
    hyo2_5 text,
    hyo2_6 text,
    hyo2_7 text,
    hyo2_8 text,
    hyo2_9 text,
    hyo2_10 text,
    hyo2_11 text,
    hyo2_12 text,
    hyo2_13 text,
    hyo2_14 text,
    hyo2_15 text,
    hyo2_16 text,
    hyo2_17 text,
    hyo2_18 text,
    hyo2_19 text,
    hyo2_20 text,
    hyo3_1 text,
    hyo3_2 text,
    hyo3_3 text,
    hyo3_4 text,
    hyo3_5 text,
    hyo3_6 text,
    hyo3_7 text,
    hyo3_8 text,
    hyo3_9 text,
    hyo3_10 text,
    hyo3_11 text,
    hyo3_12 text,
    hyo3_13 text,
    hyo3_14 text,
    hyo3_15 text,
    hyo3_16 text,
    hyo3_17 text,
    hyo3_18 text,
    hyo3_19 text,
    hyo3_20 text,
    hyo4_1 text,
    hyo4_2 text,
    hyo4_3 text,
    hyo4_4 text,
    hyo4_5 text,
    hyo4_6 text,
    hyo4_7 text,
    hyo4_8 text,
    hyo4_9 text,
    hyo4_10 text,
    hyo4_11 text,
    hyo4_12 text,
    hyo4_13 text,
    hyo4_14 text,
    hyo4_15 text,
    hyo4_16 text,
    hyo4_17 text,
    hyo4_18 text,
    hyo4_19 text,
    hyo4_20 text,
    hyo5_1 text,
    hyo5_2 text,
    hyo5_3 text,
    hyo5_4 text,
    hyo5_5 text,
    hyo5_6 text,
    hyo5_7 text,
    hyo5_8 text,
    hyo5_9 text,
    hyo5_10 text,
    hyo5_11 text,
    hyo5_12 text,
    hyo5_13 text,
    hyo5_14 text,
    hyo5_15 text,
    hyo5_16 text,
    hyo5_17 text,
    hyo5_18 text,
    hyo5_19 text,
    hyo5_20 text,
    hyo6_1 text,
    hyo6_2 text,
    hyo6_3 text,
    hyo6_4 text,
    hyo6_5 text,
    hyo6_6 text,
    hyo6_7 text,
    hyo6_8 text,
    hyo6_9 text,
    hyo6_10 text,
    hyo6_11 text,
    hyo6_12 text,
    hyo6_13 text,
    hyo6_14 text,
    hyo6_15 text,
    hyo6_16 text,
    hyo6_17 text,
    hyo6_18 text,
    hyo6_19 text,
    hyo6_20 text,
    jiko_ttl integer,
    hyo1_ttl integer,
    hyo2_ttl integer,
    hyo3_ttl integer,
    hyo4_ttl integer,
    hyo5_ttl integer,
    hyo6_ttl integer,
    hyo9_ttl integer,
    adjust integer,
    progress integer DEFAULT 0 NOT NULL,
    is_excluded character varying(1) DEFAULT ''::character varying NOT NULL,
    remark text
);


ALTER TABLE public.phase1a_hnv_casts OWNER TO postgres;

--
-- TOC entry 1666 (class 1259 OID 54869108)
-- Dependencies: 2020 6
-- Name: phase1a_hnv_emps; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE phase1a_hnv_emps (
    empid character varying(30) NOT NULL,
    login_id character varying(100) NOT NULL,
    login_pw character varying(100),
    empnm character varying(300),
    is_admin character varying(1),
    is_active character varying(1) DEFAULT '1'::character varying NOT NULL
);


ALTER TABLE public.phase1a_hnv_emps OWNER TO postgres;

--
-- TOC entry 1665 (class 1259 OID 54869098)
-- Dependencies: 2018 2019 6
-- Name: phase1a_hnv_revisions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE phase1a_hnv_revisions (
    revid integer NOT NULL,
    revnm text,
    rev_fr_ymd character varying(10),
    rev_to_ymd character varying(10),
    hyo_level text,
    ttl_rank text,
    hyo_step integer DEFAULT 0 NOT NULL,
    hyo_listinput text,
    is_active_revision character varying(1) DEFAULT ''::character varying NOT NULL,
    hyo_remark1 text,
    hyo_remark2 text,
    hyo_remark3 text,
    hyo_remark4 text,
    hyo_remark5 text,
    hyo_remark6 text
);


ALTER TABLE public.phase1a_hnv_revisions OWNER TO postgres;

--
-- TOC entry 1663 (class 1259 OID 54869078)
-- Dependencies: 6
-- Name: phase1a_hnv_scorebody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE phase1a_hnv_scorebody (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    kouid integer NOT NULL,
    kou_title text,
    kou_teigi text,
    kou_1 text,
    kou_2 text,
    kou_3 text,
    kou_4 text,
    kou_5 text,
    kou_6 text,
    kou_7 text,
    kou_8 text,
    kou_9 text,
    kou_10 text,
    kou_11 text,
    kou_12 text,
    kou_13 text,
    kou_14 text,
    kou_15 text,
    kou_16 text,
    kou_17 text,
    kou_18 text,
    kou_19 text,
    kou_20 text,
    ten_1 text,
    ten_2 text,
    ten_3 text,
    ten_4 text,
    ten_5 text,
    ten_6 text,
    ten_7 text,
    ten_8 text,
    ten_9 text,
    ten_10 text,
    ten_11 text,
    ten_12 text,
    ten_13 text,
    ten_14 text,
    ten_15 text,
    ten_16 text,
    ten_17 text,
    ten_18 text,
    ten_19 text,
    ten_20 text
);


ALTER TABLE public.phase1a_hnv_scorebody OWNER TO postgres;

--
-- TOC entry 1662 (class 1259 OID 54869069)
-- Dependencies: 2013 6
-- Name: phase1a_hnv_scorehead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE phase1a_hnv_scorehead (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    hyonm text,
    setumonsu integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.phase1a_hnv_scorehead OWNER TO postgres;

SET default_with_oids = false;

--
-- TOC entry 1661 (class 1259 OID 54816126)
-- Dependencies: 2012 6
-- Name: takazono_hnv_empmst; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE takazono_hnv_empmst (
    empid character varying(30) NOT NULL,
    login_id character varying(100) NOT NULL,
    login_pw character varying(100),
    empnm text,
    is_admin character varying(1),
    is_active character varying(1) DEFAULT '1'::character varying NOT NULL
);


ALTER TABLE public.takazono_hnv_empmst OWNER TO postgres;

--
-- TOC entry 2143 (class 0 OID 0)
-- Dependencies: 1661
-- Name: COLUMN takazono_hnv_empmst.empid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN takazono_hnv_empmst.empid IS '職員ID';


--
-- TOC entry 2144 (class 0 OID 0)
-- Dependencies: 1661
-- Name: COLUMN takazono_hnv_empmst.login_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN takazono_hnv_empmst.login_id IS 'ログイン用のID。半角英数小文字推奨。';


--
-- TOC entry 2145 (class 0 OID 0)
-- Dependencies: 1661
-- Name: COLUMN takazono_hnv_empmst.login_pw; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN takazono_hnv_empmst.login_pw IS 'ログインパスワード';


SET default_with_oids = true;

--
-- TOC entry 1646 (class 1259 OID 53902253)
-- Dependencies: 1983 1984 1985 1986 6
-- Name: takazono_hnv_kikanemp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE takazono_hnv_kikanemp (
    revid integer NOT NULL,
    empnm character varying(200) NOT NULL,
    empid character varying(30) NOT NULL,
    syozoku text,
    syokusyu text,
    yakusyoku text,
    kihonkyu integer DEFAULT 0 NOT NULL,
    toukyu character varying(10),
    hyoid integer DEFAULT 0 NOT NULL,
    hyouka1_empid character varying(30),
    hyouka2_empid character varying(30),
    hyouka3_empid character varying(30),
    hyouka4_empid character varying(30),
    hyouka5_empid character varying(30),
    hyouka6_empid character varying(30),
    is_hyomaster character varying(1),
    jiko_1 text,
    jiko_2 text,
    jiko_3 text,
    jiko_4 text,
    jiko_5 text,
    jiko_6 text,
    jiko_7 text,
    jiko_8 text,
    jiko_9 text,
    jiko_10 text,
    jiko_11 text,
    jiko_12 text,
    jiko_13 text,
    jiko_14 text,
    jiko_15 text,
    jiko_16 text,
    jiko_17 text,
    jiko_18 text,
    jiko_19 text,
    jiko_20 text,
    hyo1_1 text,
    hyo1_2 text,
    hyo1_3 text,
    hyo1_4 text,
    hyo1_5 text,
    hyo1_6 text,
    hyo1_7 text,
    hyo1_8 text,
    hyo1_9 text,
    hyo1_10 text,
    hyo1_11 text,
    hyo1_12 text,
    hyo1_13 text,
    hyo1_14 text,
    hyo1_15 text,
    hyo1_16 text,
    hyo1_17 text,
    hyo1_18 text,
    hyo1_19 text,
    hyo1_20 text,
    hyo2_1 text,
    hyo2_2 text,
    hyo2_3 text,
    hyo2_4 text,
    hyo2_5 text,
    hyo2_6 text,
    hyo2_7 text,
    hyo2_8 text,
    hyo2_9 text,
    hyo2_10 text,
    hyo2_11 text,
    hyo2_12 text,
    hyo2_13 text,
    hyo2_14 text,
    hyo2_15 text,
    hyo2_16 text,
    hyo2_17 text,
    hyo2_18 text,
    hyo2_19 text,
    hyo2_20 text,
    hyo3_1 text,
    hyo3_2 text,
    hyo3_3 text,
    hyo3_4 text,
    hyo3_5 text,
    hyo3_6 text,
    hyo3_7 text,
    hyo3_8 text,
    hyo3_9 text,
    hyo3_10 text,
    hyo3_11 text,
    hyo3_12 text,
    hyo3_13 text,
    hyo3_14 text,
    hyo3_15 text,
    hyo3_16 text,
    hyo3_17 text,
    hyo3_18 text,
    hyo3_19 text,
    hyo3_20 text,
    hyo4_1 text,
    hyo4_2 text,
    hyo4_3 text,
    hyo4_4 text,
    hyo4_5 text,
    hyo4_6 text,
    hyo4_7 text,
    hyo4_8 text,
    hyo4_9 text,
    hyo4_10 text,
    hyo4_11 text,
    hyo4_12 text,
    hyo4_13 text,
    hyo4_14 text,
    hyo4_15 text,
    hyo4_16 text,
    hyo4_17 text,
    hyo4_18 text,
    hyo4_19 text,
    hyo4_20 text,
    hyo5_1 text,
    hyo5_2 text,
    hyo5_3 text,
    hyo5_4 text,
    hyo5_5 text,
    hyo5_6 text,
    hyo5_7 text,
    hyo5_8 text,
    hyo5_9 text,
    hyo5_10 text,
    hyo5_11 text,
    hyo5_12 text,
    hyo5_13 text,
    hyo5_14 text,
    hyo5_15 text,
    hyo5_16 text,
    hyo5_17 text,
    hyo5_18 text,
    hyo5_19 text,
    hyo5_20 text,
    hyo6_1 text,
    hyo6_2 text,
    hyo6_3 text,
    hyo6_4 text,
    hyo6_5 text,
    hyo6_6 text,
    hyo6_7 text,
    hyo6_8 text,
    hyo6_9 text,
    hyo6_10 text,
    hyo6_11 text,
    hyo6_12 text,
    hyo6_13 text,
    hyo6_14 text,
    hyo6_15 text,
    hyo6_16 text,
    hyo6_17 text,
    hyo6_18 text,
    hyo6_19 text,
    hyo6_20 text,
    jiko_ttl integer,
    hyo1_ttl integer,
    hyo2_ttl integer,
    hyo3_ttl integer,
    hyo4_ttl integer,
    hyo5_ttl integer,
    hyo6_ttl integer,
    hyo9_ttl integer,
    adjust integer,
    progress integer DEFAULT 0 NOT NULL,
    is_excluded character varying(1) DEFAULT ''::character varying NOT NULL,
    remark text,
    hyo1_remark text,
    hyo2_remark text,
    hyo3_remark text,
    hyo4_remark text,
    hyo5_remark text,
    hyo6_remark text,
    kojin_hyo_step integer,
    kakutei_score integer,
    kakutei_rank integer
);


ALTER TABLE public.takazono_hnv_kikanemp OWNER TO postgres;

--
-- TOC entry 1649 (class 1259 OID 53902285)
-- Dependencies: 1988 1989 6
-- Name: takazono_hnv_revisions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE takazono_hnv_revisions (
    revid integer NOT NULL,
    revnm text,
    rev_fr_ymd character varying(10),
    rev_to_ymd character varying(10),
    hyo_level text,
    ttl_rank text,
    hyo_step integer DEFAULT 0 NOT NULL,
    hyo_listinput text,
    is_active_revision character varying(1) DEFAULT ''::character varying NOT NULL,
    hyo_remark1 text,
    hyo_remark2 text,
    hyo_remark3 text,
    hyo_remark4 text,
    hyo_remark5 text,
    hyo_remark6 text,
    is_finished character varying(1)
);


ALTER TABLE public.takazono_hnv_revisions OWNER TO postgres;

--
-- TOC entry 1647 (class 1259 OID 53902263)
-- Dependencies: 6
-- Name: takazono_hnv_scorebody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE takazono_hnv_scorebody (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    kouid integer NOT NULL,
    kou_title text,
    kou_teigi text,
    kou_1 text,
    kou_2 text,
    kou_3 text,
    kou_4 text,
    kou_5 text,
    kou_6 text,
    kou_7 text,
    kou_8 text,
    kou_9 text,
    kou_10 text,
    kou_11 text,
    kou_12 text,
    kou_13 text,
    kou_14 text,
    kou_15 text,
    kou_16 text,
    kou_17 text,
    kou_18 text,
    kou_19 text,
    kou_20 text,
    ten_1 text,
    ten_2 text,
    ten_3 text,
    ten_4 text,
    ten_5 text,
    ten_6 text,
    ten_7 text,
    ten_8 text,
    ten_9 text,
    ten_10 text,
    ten_11 text,
    ten_12 text,
    ten_13 text,
    ten_14 text,
    ten_15 text,
    ten_16 text,
    ten_17 text,
    ten_18 text,
    ten_19 text,
    ten_20 text
);


ALTER TABLE public.takazono_hnv_scorebody OWNER TO postgres;

--
-- TOC entry 1648 (class 1259 OID 53902269)
-- Dependencies: 1987 6
-- Name: takazono_hnv_scorehead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE takazono_hnv_scorehead (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    hyonm text,
    setumonsu integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.takazono_hnv_scorehead OWNER TO postgres;

--
-- TOC entry 1650 (class 1259 OID 53902293)
-- Dependencies: 1990 1991 1992 1993 1994 1995 1996 1997 1998 1999 2000 6
-- Name: test1_nkjk_emps; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test1_nkjk_emps (
    revid integer NOT NULL,
    empnm character varying(200) NOT NULL,
    emp2nm character varying(200) NOT NULL,
    emp_personal_id character varying(12),
    syozoku text,
    syokusyu text,
    yakusyoku text,
    kihonkyu integer DEFAULT 0 NOT NULL,
    toukyu character varying(10),
    hyoid integer DEFAULT 0 NOT NULL,
    hyouka1_empnm character varying(200),
    hyouka2_empnm character varying(200),
    hyouka3_empnm character varying(200),
    hyouka4_empnm character varying(200),
    hyouka5_empnm character varying(200),
    hyouka6_empnm character varying(200),
    hyouka1_emp2nm character varying(200),
    hyouka2_emp2nm character varying(200),
    hyouka3_emp2nm character varying(200),
    hyouka4_emp2nm character varying(200),
    hyouka5_emp2nm character varying(200),
    hyouka6_emp2nm character varying(200),
    is_hyomaster character varying(1),
    jiko_1 text,
    jiko_2 text,
    jiko_3 text,
    jiko_4 text,
    jiko_5 text,
    jiko_6 text,
    jiko_7 text,
    jiko_8 text,
    jiko_9 text,
    jiko_10 text,
    jiko_11 text,
    jiko_12 text,
    jiko_13 text,
    jiko_14 text,
    jiko_15 text,
    jiko_16 text,
    jiko_17 text,
    jiko_18 text,
    jiko_19 text,
    jiko_20 text,
    hyo1_1 text,
    hyo1_2 text,
    hyo1_3 text,
    hyo1_4 text,
    hyo1_5 text,
    hyo1_6 text,
    hyo1_7 text,
    hyo1_8 text,
    hyo1_9 text,
    hyo1_10 text,
    hyo1_11 text,
    hyo1_12 text,
    hyo1_13 text,
    hyo1_14 text,
    hyo1_15 text,
    hyo1_16 text,
    hyo1_17 text,
    hyo1_18 text,
    hyo1_19 text,
    hyo1_20 text,
    hyo2_1 text,
    hyo2_2 text,
    hyo2_3 text,
    hyo2_4 text,
    hyo2_5 text,
    hyo2_6 text,
    hyo2_7 text,
    hyo2_8 text,
    hyo2_9 text,
    hyo2_10 text,
    hyo2_11 text,
    hyo2_12 text,
    hyo2_13 text,
    hyo2_14 text,
    hyo2_15 text,
    hyo2_16 text,
    hyo2_17 text,
    hyo2_18 text,
    hyo2_19 text,
    hyo2_20 text,
    hyo3_1 text,
    hyo3_2 text,
    hyo3_3 text,
    hyo3_4 text,
    hyo3_5 text,
    hyo3_6 text,
    hyo3_7 text,
    hyo3_8 text,
    hyo3_9 text,
    hyo3_10 text,
    hyo3_11 text,
    hyo3_12 text,
    hyo3_13 text,
    hyo3_14 text,
    hyo3_15 text,
    hyo3_16 text,
    hyo3_17 text,
    hyo3_18 text,
    hyo3_19 text,
    hyo3_20 text,
    hyo4_1 text,
    hyo4_2 text,
    hyo4_3 text,
    hyo4_4 text,
    hyo4_5 text,
    hyo4_6 text,
    hyo4_7 text,
    hyo4_8 text,
    hyo4_9 text,
    hyo4_10 text,
    hyo4_11 text,
    hyo4_12 text,
    hyo4_13 text,
    hyo4_14 text,
    hyo4_15 text,
    hyo4_16 text,
    hyo4_17 text,
    hyo4_18 text,
    hyo4_19 text,
    hyo4_20 text,
    hyo5_1 text,
    hyo5_2 text,
    hyo5_3 text,
    hyo5_4 text,
    hyo5_5 text,
    hyo5_6 text,
    hyo5_7 text,
    hyo5_8 text,
    hyo5_9 text,
    hyo5_10 text,
    hyo5_11 text,
    hyo5_12 text,
    hyo5_13 text,
    hyo5_14 text,
    hyo5_15 text,
    hyo5_16 text,
    hyo5_17 text,
    hyo5_18 text,
    hyo5_19 text,
    hyo5_20 text,
    hyo6_1 text,
    hyo6_2 text,
    hyo6_3 text,
    hyo6_4 text,
    hyo6_5 text,
    hyo6_6 text,
    hyo6_7 text,
    hyo6_8 text,
    hyo6_9 text,
    hyo6_10 text,
    hyo6_11 text,
    hyo6_12 text,
    hyo6_13 text,
    hyo6_14 text,
    hyo6_15 text,
    hyo6_16 text,
    hyo6_17 text,
    hyo6_18 text,
    hyo6_19 text,
    hyo6_20 text,
    jiko_ttl integer DEFAULT 0 NOT NULL,
    hyo1_ttl integer DEFAULT 0 NOT NULL,
    hyo2_ttl integer DEFAULT 0 NOT NULL,
    hyo3_ttl integer DEFAULT 0 NOT NULL,
    hyo4_ttl integer DEFAULT 0 NOT NULL,
    hyo5_ttl integer DEFAULT 0 NOT NULL,
    hyo6_ttl integer DEFAULT 0 NOT NULL,
    progress integer DEFAULT 0 NOT NULL,
    is_excluded character varying(1) DEFAULT ''::character varying NOT NULL,
    remark text,
    hyo1_remark text,
    hyo2_remark text,
    hyo3_remark text,
    hyo4_remark text,
    hyo5_remark text,
    hyo6_remark text,
    hyo9_ttl text,
    adjust integer,
    kojin_hyo_step integer,
    kakutei_score integer,
    kakutei_rank integer
);


ALTER TABLE public.test1_nkjk_emps OWNER TO postgres;

--
-- TOC entry 1651 (class 1259 OID 53902310)
-- Dependencies: 6
-- Name: test1_nkjk_hyobody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test1_nkjk_hyobody (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    kouid integer NOT NULL,
    kou_title text,
    kou_teigi text,
    kou_1 text,
    kou_2 text,
    kou_3 text,
    kou_4 text,
    kou_5 text,
    kou_6 text,
    kou_7 text,
    kou_8 text,
    kou_9 text,
    kou_10 text,
    kou_11 text,
    kou_12 text,
    kou_13 text,
    kou_14 text,
    kou_15 text,
    kou_16 text,
    kou_17 text,
    kou_18 text,
    kou_19 text,
    kou_20 text,
    ten_1 text,
    ten_2 text,
    ten_3 text,
    ten_4 text,
    ten_5 text,
    ten_6 text,
    ten_7 text,
    ten_8 text,
    ten_9 text,
    ten_10 text,
    ten_11 text,
    ten_12 text,
    ten_13 text,
    ten_14 text,
    ten_15 text,
    ten_16 text,
    ten_17 text,
    ten_18 text,
    ten_19 text,
    ten_20 text
);


ALTER TABLE public.test1_nkjk_hyobody OWNER TO postgres;

--
-- TOC entry 1652 (class 1259 OID 53902316)
-- Dependencies: 2001 6
-- Name: test1_nkjk_hyohead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test1_nkjk_hyohead (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    hyonm text,
    setumonsu integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.test1_nkjk_hyohead OWNER TO postgres;

--
-- TOC entry 1653 (class 1259 OID 53902323)
-- Dependencies: 2002 2003 6
-- Name: test1_nkjk_revisions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test1_nkjk_revisions (
    revid integer NOT NULL,
    revnm text,
    rev_fr_ymd character varying(10),
    rev_to_ymd character varying(10),
    hyo_level text,
    ttl_rank text,
    hyo_step integer DEFAULT 0 NOT NULL,
    hyo_listinput text,
    is_active_revision character varying(1) DEFAULT ''::character varying NOT NULL,
    hyo_remark1 text,
    hyo_remark2 text,
    hyo_remark3 text,
    hyo_remark4 text,
    hyo_remark5 text,
    hyo_remark6 text,
    is_finished character varying(1)
);


ALTER TABLE public.test1_nkjk_revisions OWNER TO postgres;

--
-- TOC entry 1654 (class 1259 OID 53902331)
-- Dependencies: 2004 2005 2006 2007 6
-- Name: test2_nkjk_emps; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test2_nkjk_emps (
    revid integer NOT NULL,
    empnm character varying(200) NOT NULL,
    emp2nm character varying(200) NOT NULL,
    emp_personal_id character varying(200),
    syozoku text,
    syokusyu text,
    yakusyoku text,
    kihonkyu integer DEFAULT 0 NOT NULL,
    toukyu character varying(10),
    hyoid integer DEFAULT 0 NOT NULL,
    hyouka1_empnm character varying(200),
    hyouka2_empnm character varying(200),
    hyouka3_empnm character varying(200),
    hyouka4_empnm character varying(200),
    hyouka5_empnm character varying(200),
    hyouka6_empnm character varying(200),
    hyouka1_emp2nm character varying(200),
    hyouka2_emp2nm character varying(200),
    hyouka3_emp2nm character varying(200),
    hyouka4_emp2nm character varying(200),
    hyouka5_emp2nm character varying(200),
    hyouka6_emp2nm character varying(200),
    is_hyomaster character varying(1),
    jiko_1 text,
    jiko_2 text,
    jiko_3 text,
    jiko_4 text,
    jiko_5 text,
    jiko_6 text,
    jiko_7 text,
    jiko_8 text,
    jiko_9 text,
    jiko_10 text,
    jiko_11 text,
    jiko_12 text,
    jiko_13 text,
    jiko_14 text,
    jiko_15 text,
    jiko_16 text,
    jiko_17 text,
    jiko_18 text,
    jiko_19 text,
    jiko_20 text,
    hyo1_1 text,
    hyo1_2 text,
    hyo1_3 text,
    hyo1_4 text,
    hyo1_5 text,
    hyo1_6 text,
    hyo1_7 text,
    hyo1_8 text,
    hyo1_9 text,
    hyo1_10 text,
    hyo1_11 text,
    hyo1_12 text,
    hyo1_13 text,
    hyo1_14 text,
    hyo1_15 text,
    hyo1_16 text,
    hyo1_17 text,
    hyo1_18 text,
    hyo1_19 text,
    hyo1_20 text,
    hyo2_1 text,
    hyo2_2 text,
    hyo2_3 text,
    hyo2_4 text,
    hyo2_5 text,
    hyo2_6 text,
    hyo2_7 text,
    hyo2_8 text,
    hyo2_9 text,
    hyo2_10 text,
    hyo2_11 text,
    hyo2_12 text,
    hyo2_13 text,
    hyo2_14 text,
    hyo2_15 text,
    hyo2_16 text,
    hyo2_17 text,
    hyo2_18 text,
    hyo2_19 text,
    hyo2_20 text,
    hyo3_1 text,
    hyo3_2 text,
    hyo3_3 text,
    hyo3_4 text,
    hyo3_5 text,
    hyo3_6 text,
    hyo3_7 text,
    hyo3_8 text,
    hyo3_9 text,
    hyo3_10 text,
    hyo3_11 text,
    hyo3_12 text,
    hyo3_13 text,
    hyo3_14 text,
    hyo3_15 text,
    hyo3_16 text,
    hyo3_17 text,
    hyo3_18 text,
    hyo3_19 text,
    hyo3_20 text,
    hyo4_1 text,
    hyo4_2 text,
    hyo4_3 text,
    hyo4_4 text,
    hyo4_5 text,
    hyo4_6 text,
    hyo4_7 text,
    hyo4_8 text,
    hyo4_9 text,
    hyo4_10 text,
    hyo4_11 text,
    hyo4_12 text,
    hyo4_13 text,
    hyo4_14 text,
    hyo4_15 text,
    hyo4_16 text,
    hyo4_17 text,
    hyo4_18 text,
    hyo4_19 text,
    hyo4_20 text,
    hyo5_1 text,
    hyo5_2 text,
    hyo5_3 text,
    hyo5_4 text,
    hyo5_5 text,
    hyo5_6 text,
    hyo5_7 text,
    hyo5_8 text,
    hyo5_9 text,
    hyo5_10 text,
    hyo5_11 text,
    hyo5_12 text,
    hyo5_13 text,
    hyo5_14 text,
    hyo5_15 text,
    hyo5_16 text,
    hyo5_17 text,
    hyo5_18 text,
    hyo5_19 text,
    hyo5_20 text,
    hyo6_1 text,
    hyo6_2 text,
    hyo6_3 text,
    hyo6_4 text,
    hyo6_5 text,
    hyo6_6 text,
    hyo6_7 text,
    hyo6_8 text,
    hyo6_9 text,
    hyo6_10 text,
    hyo6_11 text,
    hyo6_12 text,
    hyo6_13 text,
    hyo6_14 text,
    hyo6_15 text,
    hyo6_16 text,
    hyo6_17 text,
    hyo6_18 text,
    hyo6_19 text,
    hyo6_20 text,
    jiko_ttl integer,
    hyo1_ttl integer,
    hyo2_ttl integer,
    hyo3_ttl integer,
    hyo4_ttl integer,
    hyo5_ttl integer,
    hyo6_ttl integer,
    hyo9_ttl integer,
    adjust integer,
    progress integer DEFAULT 0 NOT NULL,
    is_excluded character varying(1) DEFAULT ''::character varying NOT NULL,
    remark text,
    kojin_hyo_step integer,
    kakutei_score integer,
    kakutei_rank integer
);


ALTER TABLE public.test2_nkjk_emps OWNER TO postgres;

--
-- TOC entry 1655 (class 1259 OID 53902341)
-- Dependencies: 6
-- Name: test2_nkjk_hyobody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test2_nkjk_hyobody (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    kouid integer NOT NULL,
    kou_title text,
    kou_teigi text,
    kou_1 text,
    kou_2 text,
    kou_3 text,
    kou_4 text,
    kou_5 text,
    kou_6 text,
    kou_7 text,
    kou_8 text,
    kou_9 text,
    kou_10 text,
    kou_11 text,
    kou_12 text,
    kou_13 text,
    kou_14 text,
    kou_15 text,
    kou_16 text,
    kou_17 text,
    kou_18 text,
    kou_19 text,
    kou_20 text,
    ten_1 text,
    ten_2 text,
    ten_3 text,
    ten_4 text,
    ten_5 text,
    ten_6 text,
    ten_7 text,
    ten_8 text,
    ten_9 text,
    ten_10 text,
    ten_11 text,
    ten_12 text,
    ten_13 text,
    ten_14 text,
    ten_15 text,
    ten_16 text,
    ten_17 text,
    ten_18 text,
    ten_19 text,
    ten_20 text
);


ALTER TABLE public.test2_nkjk_hyobody OWNER TO postgres;

--
-- TOC entry 1656 (class 1259 OID 53902347)
-- Dependencies: 2008 6
-- Name: test2_nkjk_hyohead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test2_nkjk_hyohead (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    hyonm text,
    setumonsu integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.test2_nkjk_hyohead OWNER TO postgres;

--
-- TOC entry 1657 (class 1259 OID 53902354)
-- Dependencies: 6
-- Name: test2_nkjk_kyubody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test2_nkjk_kyubody (
    kyuid integer NOT NULL,
    rowidx integer NOT NULL,
    goubou numeric(6,3),
    toukyu1yen integer,
    toukyu2yen integer,
    toukyu3yen integer,
    toukyu4yen integer,
    toukyu5yen integer,
    toukyu6yen integer,
    toukyu7yen integer,
    toukyu8yen integer,
    toukyu9yen integer,
    toukyu10yen integer
);


ALTER TABLE public.test2_nkjk_kyubody OWNER TO postgres;

--
-- TOC entry 1658 (class 1259 OID 53902357)
-- Dependencies: 6
-- Name: test2_nkjk_kyuhead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test2_nkjk_kyuhead (
    kyuid integer NOT NULL,
    kyunm text,
    toukyu_count integer
);


ALTER TABLE public.test2_nkjk_kyuhead OWNER TO postgres;

--
-- TOC entry 1659 (class 1259 OID 53902363)
-- Dependencies: 2009 2010 6
-- Name: test2_nkjk_revisions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test2_nkjk_revisions (
    revid integer NOT NULL,
    revnm text,
    rev_fr_ymd character varying(10),
    rev_to_ymd character varying(10),
    hyo_level text,
    ttl_rank text,
    hyo_step integer DEFAULT 0 NOT NULL,
    hyo_listinput text,
    is_active_revision character varying(1) DEFAULT ''::character varying NOT NULL,
    hyo_remark1 text,
    hyo_remark2 text,
    hyo_remark3 text,
    hyo_remark4 text,
    hyo_remark5 text,
    hyo_remark6 text,
    is_finished character varying(1)
);


ALTER TABLE public.test2_nkjk_revisions OWNER TO postgres;

--
-- TOC entry 1669 (class 1259 OID 54947975)
-- Dependencies: 2022 2023 2024 6
-- Name: test4_hnv_casts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test4_hnv_casts (
    revid integer NOT NULL,
    empid character varying(30) NOT NULL,
    empnm character varying(300) NOT NULL,
    syozoku text,
    syokusyu text,
    yakusyoku text,
    toukyu character varying(10),
    hyoid integer DEFAULT 0 NOT NULL,
    hyouka1_empid character varying(30),
    hyouka2_empid character varying(30),
    hyouka3_empid character varying(30),
    hyouka4_empid character varying(30),
    hyouka5_empid character varying(30),
    hyouka6_empid character varying(30),
    hyouka1_empnm character varying(300),
    hyouka2_empnm character varying(300),
    hyouka3_empnm character varying(300),
    hyouka4_empnm character varying(300),
    hyouka5_empnm character varying(300),
    hyouka6_empnm character varying(300),
    is_hyomaster character varying(1),
    kojin_hyo_step integer,
    kakutei_score integer,
    kakutei_rank integer,
    jiko_1 text,
    jiko_2 text,
    jiko_3 text,
    jiko_4 text,
    jiko_5 text,
    jiko_6 text,
    jiko_7 text,
    jiko_8 text,
    jiko_9 text,
    jiko_10 text,
    jiko_11 text,
    jiko_12 text,
    jiko_13 text,
    jiko_14 text,
    jiko_15 text,
    jiko_16 text,
    jiko_17 text,
    jiko_18 text,
    jiko_19 text,
    jiko_20 text,
    hyo1_1 text,
    hyo1_2 text,
    hyo1_3 text,
    hyo1_4 text,
    hyo1_5 text,
    hyo1_6 text,
    hyo1_7 text,
    hyo1_8 text,
    hyo1_9 text,
    hyo1_10 text,
    hyo1_11 text,
    hyo1_12 text,
    hyo1_13 text,
    hyo1_14 text,
    hyo1_15 text,
    hyo1_16 text,
    hyo1_17 text,
    hyo1_18 text,
    hyo1_19 text,
    hyo1_20 text,
    hyo2_1 text,
    hyo2_2 text,
    hyo2_3 text,
    hyo2_4 text,
    hyo2_5 text,
    hyo2_6 text,
    hyo2_7 text,
    hyo2_8 text,
    hyo2_9 text,
    hyo2_10 text,
    hyo2_11 text,
    hyo2_12 text,
    hyo2_13 text,
    hyo2_14 text,
    hyo2_15 text,
    hyo2_16 text,
    hyo2_17 text,
    hyo2_18 text,
    hyo2_19 text,
    hyo2_20 text,
    hyo3_1 text,
    hyo3_2 text,
    hyo3_3 text,
    hyo3_4 text,
    hyo3_5 text,
    hyo3_6 text,
    hyo3_7 text,
    hyo3_8 text,
    hyo3_9 text,
    hyo3_10 text,
    hyo3_11 text,
    hyo3_12 text,
    hyo3_13 text,
    hyo3_14 text,
    hyo3_15 text,
    hyo3_16 text,
    hyo3_17 text,
    hyo3_18 text,
    hyo3_19 text,
    hyo3_20 text,
    hyo4_1 text,
    hyo4_2 text,
    hyo4_3 text,
    hyo4_4 text,
    hyo4_5 text,
    hyo4_6 text,
    hyo4_7 text,
    hyo4_8 text,
    hyo4_9 text,
    hyo4_10 text,
    hyo4_11 text,
    hyo4_12 text,
    hyo4_13 text,
    hyo4_14 text,
    hyo4_15 text,
    hyo4_16 text,
    hyo4_17 text,
    hyo4_18 text,
    hyo4_19 text,
    hyo4_20 text,
    hyo5_1 text,
    hyo5_2 text,
    hyo5_3 text,
    hyo5_4 text,
    hyo5_5 text,
    hyo5_6 text,
    hyo5_7 text,
    hyo5_8 text,
    hyo5_9 text,
    hyo5_10 text,
    hyo5_11 text,
    hyo5_12 text,
    hyo5_13 text,
    hyo5_14 text,
    hyo5_15 text,
    hyo5_16 text,
    hyo5_17 text,
    hyo5_18 text,
    hyo5_19 text,
    hyo5_20 text,
    hyo6_1 text,
    hyo6_2 text,
    hyo6_3 text,
    hyo6_4 text,
    hyo6_5 text,
    hyo6_6 text,
    hyo6_7 text,
    hyo6_8 text,
    hyo6_9 text,
    hyo6_10 text,
    hyo6_11 text,
    hyo6_12 text,
    hyo6_13 text,
    hyo6_14 text,
    hyo6_15 text,
    hyo6_16 text,
    hyo6_17 text,
    hyo6_18 text,
    hyo6_19 text,
    hyo6_20 text,
    jiko_ttl integer,
    hyo1_ttl integer,
    hyo2_ttl integer,
    hyo3_ttl integer,
    hyo4_ttl integer,
    hyo5_ttl integer,
    hyo6_ttl integer,
    hyo9_ttl integer,
    hyo1_remark text,
    hyo2_remark text,
    hyo3_remark text,
    hyo4_remark text,
    hyo5_remark text,
    hyo6_remark text,
    adjust integer,
    progress integer DEFAULT 0 NOT NULL,
    is_excluded character varying(1) DEFAULT ''::character varying NOT NULL,
    remark text
);


ALTER TABLE public.test4_hnv_casts OWNER TO postgres;

--
-- TOC entry 1671 (class 1259 OID 54947996)
-- Dependencies: 2027 6
-- Name: test4_hnv_emps; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test4_hnv_emps (
    empid character varying(30) NOT NULL,
    login_id character varying(100) NOT NULL,
    login_pw character varying(100),
    empnm character varying(300),
    is_admin character varying(1),
    is_active character varying(1) DEFAULT '1'::character varying NOT NULL
);


ALTER TABLE public.test4_hnv_emps OWNER TO postgres;

--
-- TOC entry 1670 (class 1259 OID 54947986)
-- Dependencies: 2025 2026 6
-- Name: test4_hnv_revisions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test4_hnv_revisions (
    revid integer NOT NULL,
    revnm text,
    rev_fr_ymd character varying(10),
    rev_to_ymd character varying(10),
    hyo_level text,
    ttl_rank text,
    hyo_step integer DEFAULT 0 NOT NULL,
    hyo_listinput text,
    is_active_revision character varying(1) DEFAULT ''::character varying NOT NULL,
    hyo_remark1 text,
    hyo_remark2 text,
    hyo_remark3 text,
    hyo_remark4 text,
    hyo_remark5 text,
    hyo_remark6 text,
    is_finished character varying(1)
);


ALTER TABLE public.test4_hnv_revisions OWNER TO postgres;

--
-- TOC entry 1668 (class 1259 OID 54947967)
-- Dependencies: 6
-- Name: test4_hnv_scorebody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test4_hnv_scorebody (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    kouid integer NOT NULL,
    kou_title text,
    kou_teigi text,
    kou_1 text,
    kou_2 text,
    kou_3 text,
    kou_4 text,
    kou_5 text,
    kou_6 text,
    kou_7 text,
    kou_8 text,
    kou_9 text,
    kou_10 text,
    kou_11 text,
    kou_12 text,
    kou_13 text,
    kou_14 text,
    kou_15 text,
    kou_16 text,
    kou_17 text,
    kou_18 text,
    kou_19 text,
    kou_20 text,
    ten_1 text,
    ten_2 text,
    ten_3 text,
    ten_4 text,
    ten_5 text,
    ten_6 text,
    ten_7 text,
    ten_8 text,
    ten_9 text,
    ten_10 text,
    ten_11 text,
    ten_12 text,
    ten_13 text,
    ten_14 text,
    ten_15 text,
    ten_16 text,
    ten_17 text,
    ten_18 text,
    ten_19 text,
    ten_20 text
);


ALTER TABLE public.test4_hnv_scorebody OWNER TO postgres;

--
-- TOC entry 1667 (class 1259 OID 54947958)
-- Dependencies: 2021 6
-- Name: test4_hnv_scorehead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test4_hnv_scorehead (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    hyonm text,
    setumonsu integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.test4_hnv_scorehead OWNER TO postgres;

--
-- TOC entry 1674 (class 1259 OID 54948257)
-- Dependencies: 2029 2030 2031 6
-- Name: test_hnv_casts; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test_hnv_casts (
    revid integer NOT NULL,
    empid character varying(30) NOT NULL,
    empnm character varying(300) NOT NULL,
    syozoku text,
    syokusyu text,
    yakusyoku text,
    toukyu character varying(10),
    hyoid integer DEFAULT 0 NOT NULL,
    hyouka1_empid character varying(30),
    hyouka2_empid character varying(30),
    hyouka3_empid character varying(30),
    hyouka4_empid character varying(30),
    hyouka5_empid character varying(30),
    hyouka6_empid character varying(30),
    hyouka1_empnm character varying(300),
    hyouka2_empnm character varying(300),
    hyouka3_empnm character varying(300),
    hyouka4_empnm character varying(300),
    hyouka5_empnm character varying(300),
    hyouka6_empnm character varying(300),
    is_hyomaster character varying(1),
    kojin_hyo_step integer,
    kakutei_score integer,
    kakutei_rank integer,
    jiko_1 text,
    jiko_2 text,
    jiko_3 text,
    jiko_4 text,
    jiko_5 text,
    jiko_6 text,
    jiko_7 text,
    jiko_8 text,
    jiko_9 text,
    jiko_10 text,
    jiko_11 text,
    jiko_12 text,
    jiko_13 text,
    jiko_14 text,
    jiko_15 text,
    jiko_16 text,
    jiko_17 text,
    jiko_18 text,
    jiko_19 text,
    jiko_20 text,
    hyo1_1 text,
    hyo1_2 text,
    hyo1_3 text,
    hyo1_4 text,
    hyo1_5 text,
    hyo1_6 text,
    hyo1_7 text,
    hyo1_8 text,
    hyo1_9 text,
    hyo1_10 text,
    hyo1_11 text,
    hyo1_12 text,
    hyo1_13 text,
    hyo1_14 text,
    hyo1_15 text,
    hyo1_16 text,
    hyo1_17 text,
    hyo1_18 text,
    hyo1_19 text,
    hyo1_20 text,
    hyo2_1 text,
    hyo2_2 text,
    hyo2_3 text,
    hyo2_4 text,
    hyo2_5 text,
    hyo2_6 text,
    hyo2_7 text,
    hyo2_8 text,
    hyo2_9 text,
    hyo2_10 text,
    hyo2_11 text,
    hyo2_12 text,
    hyo2_13 text,
    hyo2_14 text,
    hyo2_15 text,
    hyo2_16 text,
    hyo2_17 text,
    hyo2_18 text,
    hyo2_19 text,
    hyo2_20 text,
    hyo3_1 text,
    hyo3_2 text,
    hyo3_3 text,
    hyo3_4 text,
    hyo3_5 text,
    hyo3_6 text,
    hyo3_7 text,
    hyo3_8 text,
    hyo3_9 text,
    hyo3_10 text,
    hyo3_11 text,
    hyo3_12 text,
    hyo3_13 text,
    hyo3_14 text,
    hyo3_15 text,
    hyo3_16 text,
    hyo3_17 text,
    hyo3_18 text,
    hyo3_19 text,
    hyo3_20 text,
    hyo4_1 text,
    hyo4_2 text,
    hyo4_3 text,
    hyo4_4 text,
    hyo4_5 text,
    hyo4_6 text,
    hyo4_7 text,
    hyo4_8 text,
    hyo4_9 text,
    hyo4_10 text,
    hyo4_11 text,
    hyo4_12 text,
    hyo4_13 text,
    hyo4_14 text,
    hyo4_15 text,
    hyo4_16 text,
    hyo4_17 text,
    hyo4_18 text,
    hyo4_19 text,
    hyo4_20 text,
    hyo5_1 text,
    hyo5_2 text,
    hyo5_3 text,
    hyo5_4 text,
    hyo5_5 text,
    hyo5_6 text,
    hyo5_7 text,
    hyo5_8 text,
    hyo5_9 text,
    hyo5_10 text,
    hyo5_11 text,
    hyo5_12 text,
    hyo5_13 text,
    hyo5_14 text,
    hyo5_15 text,
    hyo5_16 text,
    hyo5_17 text,
    hyo5_18 text,
    hyo5_19 text,
    hyo5_20 text,
    hyo6_1 text,
    hyo6_2 text,
    hyo6_3 text,
    hyo6_4 text,
    hyo6_5 text,
    hyo6_6 text,
    hyo6_7 text,
    hyo6_8 text,
    hyo6_9 text,
    hyo6_10 text,
    hyo6_11 text,
    hyo6_12 text,
    hyo6_13 text,
    hyo6_14 text,
    hyo6_15 text,
    hyo6_16 text,
    hyo6_17 text,
    hyo6_18 text,
    hyo6_19 text,
    hyo6_20 text,
    jiko_ttl integer,
    hyo1_ttl integer,
    hyo2_ttl integer,
    hyo3_ttl integer,
    hyo4_ttl integer,
    hyo5_ttl integer,
    hyo6_ttl integer,
    hyo9_ttl integer,
    hyo1_remark text,
    hyo2_remark text,
    hyo3_remark text,
    hyo4_remark text,
    hyo5_remark text,
    hyo6_remark text,
    adjust integer,
    progress integer DEFAULT 0 NOT NULL,
    is_excluded character varying(1) DEFAULT ''::character varying NOT NULL,
    remark text
);


ALTER TABLE public.test_hnv_casts OWNER TO postgres;

--
-- TOC entry 1676 (class 1259 OID 54948278)
-- Dependencies: 2034 6
-- Name: test_hnv_emps; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test_hnv_emps (
    empid character varying(30) NOT NULL,
    login_id character varying(100) NOT NULL,
    login_pw character varying(100),
    empnm character varying(300),
    is_admin character varying(1),
    is_active character varying(1) DEFAULT '1'::character varying NOT NULL
);


ALTER TABLE public.test_hnv_emps OWNER TO postgres;

--
-- TOC entry 1675 (class 1259 OID 54948268)
-- Dependencies: 2032 2033 6
-- Name: test_hnv_revisions; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test_hnv_revisions (
    revid integer NOT NULL,
    revnm text,
    rev_fr_ymd character varying(10),
    rev_to_ymd character varying(10),
    hyo_level text,
    ttl_rank text,
    hyo_step integer DEFAULT 0 NOT NULL,
    hyo_listinput text,
    is_active_revision character varying(1) DEFAULT ''::character varying NOT NULL,
    hyo_remark1 text,
    hyo_remark2 text,
    hyo_remark3 text,
    hyo_remark4 text,
    hyo_remark5 text,
    hyo_remark6 text,
    is_finished character varying(1)
);


ALTER TABLE public.test_hnv_revisions OWNER TO postgres;

--
-- TOC entry 1673 (class 1259 OID 54948249)
-- Dependencies: 6
-- Name: test_hnv_scorebody; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test_hnv_scorebody (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    kouid integer NOT NULL,
    kou_title text,
    kou_teigi text,
    kou_1 text,
    kou_2 text,
    kou_3 text,
    kou_4 text,
    kou_5 text,
    kou_6 text,
    kou_7 text,
    kou_8 text,
    kou_9 text,
    kou_10 text,
    kou_11 text,
    kou_12 text,
    kou_13 text,
    kou_14 text,
    kou_15 text,
    kou_16 text,
    kou_17 text,
    kou_18 text,
    kou_19 text,
    kou_20 text,
    ten_1 text,
    ten_2 text,
    ten_3 text,
    ten_4 text,
    ten_5 text,
    ten_6 text,
    ten_7 text,
    ten_8 text,
    ten_9 text,
    ten_10 text,
    ten_11 text,
    ten_12 text,
    ten_13 text,
    ten_14 text,
    ten_15 text,
    ten_16 text,
    ten_17 text,
    ten_18 text,
    ten_19 text,
    ten_20 text
);


ALTER TABLE public.test_hnv_scorebody OWNER TO postgres;

--
-- TOC entry 1672 (class 1259 OID 54948240)
-- Dependencies: 2028 6
-- Name: test_hnv_scorehead; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test_hnv_scorehead (
    revid integer NOT NULL,
    hyoid integer NOT NULL,
    hyonm text,
    setumonsu integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.test_hnv_scorehead OWNER TO postgres;

--
-- TOC entry 2036 (class 2606 OID 53905666)
-- Dependencies: 1629 1629 1629
-- Name: byouin2_nkjk_emps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY byouin2_nkjk_emps
    ADD CONSTRAINT byouin2_nkjk_emps_pkey PRIMARY KEY (revid, empnm);


--
-- TOC entry 2042 (class 2606 OID 53905668)
-- Dependencies: 1632 1632
-- Name: byouin2_nkjk_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY byouin2_nkjk_revisions
    ADD CONSTRAINT byouin2_nkjk_revisions_pkey PRIMARY KEY (revid);


--
-- TOC entry 2038 (class 2606 OID 53905670)
-- Dependencies: 1630 1630 1630 1630
-- Name: byouin2nkjk_hyobody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY byouin2_nkjk_hyobody
    ADD CONSTRAINT byouin2nkjk_hyobody_pkey PRIMARY KEY (revid, hyoid, kouid);


--
-- TOC entry 2040 (class 2606 OID 53905672)
-- Dependencies: 1631 1631 1631
-- Name: byouin2nkjk_hyohead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY byouin2_nkjk_hyohead
    ADD CONSTRAINT byouin2nkjk_hyohead_pkey PRIMARY KEY (revid, hyoid);


--
-- TOC entry 2044 (class 2606 OID 53905675)
-- Dependencies: 1633 1633 1633
-- Name: mss_nkjk_emps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mss_nkjk_emps
    ADD CONSTRAINT mss_nkjk_emps_pkey PRIMARY KEY (revid, empnm);


--
-- TOC entry 2046 (class 2606 OID 53905677)
-- Dependencies: 1634 1634 1634 1634
-- Name: mss_nkjk_hyobody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mss_nkjk_hyobody
    ADD CONSTRAINT mss_nkjk_hyobody_pkey PRIMARY KEY (revid, hyoid, kouid);


--
-- TOC entry 2048 (class 2606 OID 53905679)
-- Dependencies: 1635 1635 1635
-- Name: mss_nkjk_hyohead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mss_nkjk_hyohead
    ADD CONSTRAINT mss_nkjk_hyohead_pkey PRIMARY KEY (revid, hyoid);


--
-- TOC entry 2050 (class 2606 OID 53905685)
-- Dependencies: 1636 1636
-- Name: mss_nkjk_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mss_nkjk_revisions
    ADD CONSTRAINT mss_nkjk_revisions_pkey PRIMARY KEY (revid);


--
-- TOC entry 2096 (class 2606 OID 54816116)
-- Dependencies: 1660 1660
-- Name: nk_jhnv_empmst_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nk_hnv_emps
    ADD CONSTRAINT nk_jhnv_empmst_pkey PRIMARY KEY (empid);


--
-- TOC entry 2052 (class 2606 OID 54816125)
-- Dependencies: 1637 1637 1637
-- Name: nk_nkjk_emps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nk_hnv_casts
    ADD CONSTRAINT nk_nkjk_emps_pkey PRIMARY KEY (revid, empid);


--
-- TOC entry 2058 (class 2606 OID 53905694)
-- Dependencies: 1640 1640
-- Name: nk_nkjk_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nk_hnv_revisions
    ADD CONSTRAINT nk_nkjk_revisions_pkey PRIMARY KEY (revid);


--
-- TOC entry 2060 (class 2606 OID 53905696)
-- Dependencies: 1641 1641
-- Name: nkjk_clients_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hnv_clients
    ADD CONSTRAINT nkjk_clients_pkey PRIMARY KEY (cid);


--
-- TOC entry 2062 (class 2606 OID 53905698)
-- Dependencies: 1642 1642 1642
-- Name: nkjk_login_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hnv_login
    ADD CONSTRAINT nkjk_login_pkey PRIMARY KEY (cid, login_id);


--
-- TOC entry 2064 (class 2606 OID 53905700)
-- Dependencies: 1643 1643
-- Name: nkjk_templates_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hnv_templates
    ADD CONSTRAINT nkjk_templates_pkey PRIMARY KEY (tmplid);


--
-- TOC entry 2066 (class 2606 OID 53905702)
-- Dependencies: 1644 1644
-- Name: nkjk_upload_csv_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hnv_upload_csv
    ADD CONSTRAINT nkjk_upload_csv_pkey PRIMARY KEY (upload_seq);


--
-- TOC entry 2054 (class 2606 OID 53905704)
-- Dependencies: 1638 1638 1638 1638
-- Name: nknkjk_hyobody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nk_hnv_scorebody
    ADD CONSTRAINT nknkjk_hyobody_pkey PRIMARY KEY (revid, hyoid, kouid);


--
-- TOC entry 2056 (class 2606 OID 53905706)
-- Dependencies: 1639 1639 1639
-- Name: nknkjk_hyohead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY nk_hnv_scorehead
    ADD CONSTRAINT nknkjk_hyohead_pkey PRIMARY KEY (revid, hyoid);


--
-- TOC entry 2108 (class 2606 OID 54869116)
-- Dependencies: 1666 1666
-- Name: phase1a_hnv_emps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY phase1a_hnv_emps
    ADD CONSTRAINT phase1a_hnv_emps_pkey PRIMARY KEY (empid);


--
-- TOC entry 2104 (class 2606 OID 54869097)
-- Dependencies: 1664 1664 1664
-- Name: phase1a_hnv_kikansemp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY phase1a_hnv_casts
    ADD CONSTRAINT phase1a_hnv_kikansemp_pkey PRIMARY KEY (revid, empid);


--
-- TOC entry 2106 (class 2606 OID 54869107)
-- Dependencies: 1665 1665
-- Name: phase1a_hnv_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY phase1a_hnv_revisions
    ADD CONSTRAINT phase1a_hnv_revisions_pkey PRIMARY KEY (revid);


--
-- TOC entry 2102 (class 2606 OID 54869085)
-- Dependencies: 1663 1663 1663 1663
-- Name: phase1a_hnv_scorebody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY phase1a_hnv_scorebody
    ADD CONSTRAINT phase1a_hnv_scorebody_pkey PRIMARY KEY (revid, hyoid, kouid);


--
-- TOC entry 2100 (class 2606 OID 54869077)
-- Dependencies: 1662 1662 1662
-- Name: phase1a_hnv_scorehead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY phase1a_hnv_scorehead
    ADD CONSTRAINT phase1a_hnv_scorehead_pkey PRIMARY KEY (revid, hyoid);


--
-- TOC entry 2098 (class 2606 OID 54816134)
-- Dependencies: 1661 1661
-- Name: takazono_hnv_empmst_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY takazono_hnv_empmst
    ADD CONSTRAINT takazono_hnv_empmst_pkey PRIMARY KEY (empid);


--
-- TOC entry 2068 (class 2606 OID 54816143)
-- Dependencies: 1646 1646 1646
-- Name: takazono_nkjk_emps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY takazono_hnv_kikanemp
    ADD CONSTRAINT takazono_nkjk_emps_pkey PRIMARY KEY (revid, empid);


--
-- TOC entry 2070 (class 2606 OID 53905710)
-- Dependencies: 1647 1647 1647 1647
-- Name: takazono_nkjk_hyobody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY takazono_hnv_scorebody
    ADD CONSTRAINT takazono_nkjk_hyobody_pkey PRIMARY KEY (revid, hyoid, kouid);


--
-- TOC entry 2072 (class 2606 OID 53905712)
-- Dependencies: 1648 1648 1648
-- Name: takazono_nkjk_hyohead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY takazono_hnv_scorehead
    ADD CONSTRAINT takazono_nkjk_hyohead_pkey PRIMARY KEY (revid, hyoid);


--
-- TOC entry 2074 (class 2606 OID 53905718)
-- Dependencies: 1649 1649
-- Name: takazono_nkjk_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY takazono_hnv_revisions
    ADD CONSTRAINT takazono_nkjk_revisions_pkey PRIMARY KEY (revid);


--
-- TOC entry 2076 (class 2606 OID 53905720)
-- Dependencies: 1650 1650 1650
-- Name: test1_nkjk_emps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test1_nkjk_emps
    ADD CONSTRAINT test1_nkjk_emps_pkey PRIMARY KEY (revid, empnm);


--
-- TOC entry 2082 (class 2606 OID 53905722)
-- Dependencies: 1653 1653
-- Name: test1_nkjk_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test1_nkjk_revisions
    ADD CONSTRAINT test1_nkjk_revisions_pkey PRIMARY KEY (revid);


--
-- TOC entry 2078 (class 2606 OID 53905724)
-- Dependencies: 1651 1651 1651 1651
-- Name: test1nkjk_hyobody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test1_nkjk_hyobody
    ADD CONSTRAINT test1nkjk_hyobody_pkey PRIMARY KEY (revid, hyoid, kouid);


--
-- TOC entry 2080 (class 2606 OID 53905726)
-- Dependencies: 1652 1652 1652
-- Name: test1nkjk_hyohead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test1_nkjk_hyohead
    ADD CONSTRAINT test1nkjk_hyohead_pkey PRIMARY KEY (revid, hyoid);


--
-- TOC entry 2084 (class 2606 OID 53905728)
-- Dependencies: 1654 1654 1654
-- Name: test2_nkjk_emps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test2_nkjk_emps
    ADD CONSTRAINT test2_nkjk_emps_pkey PRIMARY KEY (revid, empnm);


--
-- TOC entry 2086 (class 2606 OID 53905730)
-- Dependencies: 1655 1655 1655 1655
-- Name: test2_nkjk_hyobody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test2_nkjk_hyobody
    ADD CONSTRAINT test2_nkjk_hyobody_pkey PRIMARY KEY (revid, hyoid, kouid);


--
-- TOC entry 2088 (class 2606 OID 53905732)
-- Dependencies: 1656 1656 1656
-- Name: test2_nkjk_hyohead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test2_nkjk_hyohead
    ADD CONSTRAINT test2_nkjk_hyohead_pkey PRIMARY KEY (revid, hyoid);


--
-- TOC entry 2090 (class 2606 OID 53905734)
-- Dependencies: 1657 1657 1657
-- Name: test2_nkjk_kyubody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test2_nkjk_kyubody
    ADD CONSTRAINT test2_nkjk_kyubody_pkey PRIMARY KEY (kyuid, rowidx);


--
-- TOC entry 2092 (class 2606 OID 53905736)
-- Dependencies: 1658 1658
-- Name: test2_nkjk_kyuhead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test2_nkjk_kyuhead
    ADD CONSTRAINT test2_nkjk_kyuhead_pkey PRIMARY KEY (kyuid);


--
-- TOC entry 2094 (class 2606 OID 53905738)
-- Dependencies: 1659 1659
-- Name: test2_nkjk_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test2_nkjk_revisions
    ADD CONSTRAINT test2_nkjk_revisions_pkey PRIMARY KEY (revid);


--
-- TOC entry 2118 (class 2606 OID 54948004)
-- Dependencies: 1671 1671
-- Name: test4_hnv_emps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test4_hnv_emps
    ADD CONSTRAINT test4_hnv_emps_pkey PRIMARY KEY (empid);


--
-- TOC entry 2114 (class 2606 OID 54947985)
-- Dependencies: 1669 1669 1669
-- Name: test4_hnv_kikansemp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test4_hnv_casts
    ADD CONSTRAINT test4_hnv_kikansemp_pkey PRIMARY KEY (revid, empid);


--
-- TOC entry 2116 (class 2606 OID 54947995)
-- Dependencies: 1670 1670
-- Name: test4_hnv_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test4_hnv_revisions
    ADD CONSTRAINT test4_hnv_revisions_pkey PRIMARY KEY (revid);


--
-- TOC entry 2112 (class 2606 OID 54947974)
-- Dependencies: 1668 1668 1668 1668
-- Name: test4_hnv_scorebody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test4_hnv_scorebody
    ADD CONSTRAINT test4_hnv_scorebody_pkey PRIMARY KEY (revid, hyoid, kouid);


--
-- TOC entry 2110 (class 2606 OID 54947966)
-- Dependencies: 1667 1667 1667
-- Name: test4_hnv_scorehead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test4_hnv_scorehead
    ADD CONSTRAINT test4_hnv_scorehead_pkey PRIMARY KEY (revid, hyoid);


--
-- TOC entry 2128 (class 2606 OID 54948286)
-- Dependencies: 1676 1676
-- Name: test_hnv_emps_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test_hnv_emps
    ADD CONSTRAINT test_hnv_emps_pkey PRIMARY KEY (empid);


--
-- TOC entry 2124 (class 2606 OID 54948267)
-- Dependencies: 1674 1674 1674
-- Name: test_hnv_kikansemp_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test_hnv_casts
    ADD CONSTRAINT test_hnv_kikansemp_pkey PRIMARY KEY (revid, empid);


--
-- TOC entry 2126 (class 2606 OID 54948277)
-- Dependencies: 1675 1675
-- Name: test_hnv_revisions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test_hnv_revisions
    ADD CONSTRAINT test_hnv_revisions_pkey PRIMARY KEY (revid);


--
-- TOC entry 2122 (class 2606 OID 54948256)
-- Dependencies: 1673 1673 1673 1673
-- Name: test_hnv_scorebody_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test_hnv_scorebody
    ADD CONSTRAINT test_hnv_scorebody_pkey PRIMARY KEY (revid, hyoid, kouid);


--
-- TOC entry 2120 (class 2606 OID 54948248)
-- Dependencies: 1672 1672 1672
-- Name: test_hnv_scorehead_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test_hnv_scorehead
    ADD CONSTRAINT test_hnv_scorehead_pkey PRIMARY KEY (revid, hyoid);


--
-- TOC entry 2133 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2015-06-09 08:27:32

--
-- PostgreSQL database dump complete
--

