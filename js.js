
$.ajaxSettings.type = "POST";
$.ajaxSettings.async = false;

var str = function(s) { if (!s && s!=0) return ""; return ""+s; }
var substr2=function(s,p,l){s=str(s);if(!s||s.length==0)return "";if(s.length<=p)return "";if(!l || s.length<=p+l) return s.substring(p);return s.substring(p,p+l);}
var stopEvent = function(evt) { if(!evt) evt=window.event; try{evt.stopPropagation();}catch(ex){} try{evt.preventDefault();}catch(ex){} try{evt.cancelBubble=true;}catch(ex){} return false; }
var int = function(s) { if (!s) return 0; var num = parseInt(s,10); if (!num || isNaN(num)) return 0; return num; };
var float = function(s) { if (!s) return 0; var num = parseFloat(s,10); if (!num || isNaN(num)) return 0; return num; };
var ee = function(id) { return document.getElementById(id); }
var trim = function(s) { if (s==undefined) return ""; s+=""; return s.replace(/^[\s　]+|[\s　]+$/g, ''); }
var tryFocus = function(id) { if (!id) return; var e=ee(id); tryFocusByElem(e); }
var tryFocusByElem = function(e) { if (!e) return; if(!e || e.disabled || e.readOnly) return; e.focus(); try{e.select();}catch(ex){} }
var getComboValue = function(obj) {
  return (!obj) ? "" : ( (obj.selectedIndex<0) ? "" : obj.options[obj.selectedIndex].value );
}
var getWindowScrollTop = function(win) { if ('pageYOffset' in window) return win.pageYOffset; return Math.round (win.document.documentElement.scrollTop); }

var isEnterKey = function(elem, evt){if(!elem.disabled && !elem.readOnly) var o=window.event; var key=(o?o.keyCode||o.which:evt.keyCode); return key==13; }
var setComboValue = function(obj,v, t){
    var o=obj.options;
    for(var i=0; i<o.length; i++) {
        if(o[i].value==v) {
            obj.selectedIndex=i;
            return;
        }
    }
    if (v && !t) {
        o[o.length] = new Option(v);
        obj.selectedIndex = o.length-1;
        return;
    }
    if (v && t) {
        o[o.length] = new Option(t, v);
        obj.selectedIndex = o.length-1;
        return;
    }
    obj.selectedIndex=0;
}

var AJAX_ERROR = "AJAX_ERROR";
var TYPE_JSON = "TYPE_JSON";
function getCustomTrueAjaxResult(msg, type_json, isCheckOnly) {
    var errMsg = "";
    if (msg=="SESSION_NOT_FOUND") errMsg = "画面呼び出しが不正です。（SESSION_NOT_FOUND）";
    else if (msg=="DATABASE_CONNECTION_ERROR") errMsg = "データベース接続に失敗しました。";
    else if (msg=="DATABASE_SELECT_ERROR") errMsg = "データベース検索に失敗しました。";
    else if (msg=="DATABASE_UPDATE_ERROR") errMsg = "データベース更新に失敗しました。";
    else if (msg=="SYSTEM_ERROR") errMsg = "システムエラーが発生しました。";
    else if (msg=="NO_AUTH") errMsg = "機能操作権限がありません。";
    else if (msg=="SESSION_EXPIRED") errMsg = "長時間利用されませんでしたのでタイムアウトしました。\n再度ログインしてください。";
    if (errMsg) {
        alert(errMsg);
        c2ShowLoginPage();
        return AJAX_ERROR;
    }
    if (substr2(msg,0,2)=="ng") {
        alert(msg.substring(2));
        return AJAX_ERROR;
    }
    if (isCheckOnly) return "";

    if (substr2(msg,0,2)!="ok") {
        alert(msg);
        return AJAX_ERROR;
    }
    if (type_json!=TYPE_JSON) return msg.substring(2);
    var ret = "";
    try { eval("var ret = "+msg.substring(2)+";"); } catch(ex) {
        if (ee("dump")) ee("dump").innerHTML = htmlEscape(msg);
        alert("通信エラーが発生しました。(1)\n"+msg.substring(2)); return AJAX_ERROR;
    }
    if (!ret) {
        alert("通信エラーが発生しました。(2)\n"+msg.substring(2)); return AJAX_ERROR;
    }
    return ret;
}
