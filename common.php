<?
require_once("conf.inf");
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// DBユーティリティ関連 (MDB2利用)  -- CoMedix Core2DB カスタマイズ版
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

// DB関連グローバル、スタティック扱い
class _c2db {
    var $spTypes = array();
    var $spParams = array();
    var $isRemainParams = 0;
    var $con = null;
    var $autoExecLogCategoryName = ""; // 好きに適当な値を指定すると、c2_exec_sql_logテーブルに更新系SQLの実行ログを作成します。
    var $lastErrorMsg = "";
    var $errorReturn = false;
}
global $c2db;
$c2db = new _c2db();



function _c2dbGetConnection($sql) {
    $_c2Con = MDB2::singleton(PG_DB_DSN);
    if (PEAR::isError($_c2Con)) {
        $GLOBALS["C2_ERROR_DB_CONNECTION"]=1;
        $emsg = c2dbMakeSqlErrorHtml($sql)."DB接続エラー：".c2dbDebugInfo($_c2Con);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
    }
    return $_c2Con;
}
function c2dbExecLogStart($autoExecLogCategoryName) {
    global $c2db;
    $c2db->autoExecLogCategoryName = $autoExecLogCategoryName;
}
function c2dbExecLogStop() {
    global $c2db;
    $c2db->autoExecLogCategoryName = "";
}
function c2dbDebugInfo($obj=null) {
    global $c2db;
    if (!$obj) return "";
    $err = $obj->getDebugInfo();
    $err2 = $err;
    $pos = strpos($err2, "Native message: ERROR:");
    if ($pos >=0) $err2 = substr($err2, $pos+22);
    $pos = strpos($err2, "\nLINE ");
    if ($pos >=0) $err2 = substr($err2, 0, $pos);
    $c2db->lastErrorMsg = $err2;
    return $err;
}
function c2dbRemainParams() {
    global $c2db;
    $c2db->isRemainParams = 1;
}
function c2dbGetLastErrorMsg() {
    global $c2db;
    return $c2db->lastErrorMsg;
}
function c2dbResetLastErrorMsg() {
    global $c2db;
    $c2db->lastErrorMsg = "";
}
function c2dbSetErrorReturn($bool) {
    global $c2db;
    $c2db->errorReturn = ($bool ? true : false);
}
function c2dbClearParams() {
    global $c2db;
    $c2db->spTypes = array();
    $c2db->spParams = array();
    $c2db->isRemainParams = 0;
}
function c2dbGetTableScheme($table_name) {
    $sql =
    " select pg_attribute.attname, pg_type.typname".
    " from pg_attribute, pg_type".
    " where pg_attribute.atttypid = pg_type.oid".
    " and ( pg_attribute.atttypid < 26 or pg_attribute.atttypid > 29)".
    " and attrelid in (".
    "     select pg_class.oid from pg_class, pg_namespace".
    "     where relname='".$table_name."' and pg_class.relnamespace=pg_namespace.oid".
    " )";
    $rows = c2dbGetRows($sql);
    $ret = array();
    foreach ($rows as $row) {
        $ret[$row["attname"]] = $row["typname"];
    }
    return $ret;
}

function c2dbStr($fieldValue, $fieldType = "text") {
    global $c2db;
    $fieldName = "SP".(count($c2db->spTypes) + 1);
    array_push($c2db->spTypes, $fieldType);
    if ($fieldValue=="") {
        if ($fieldType=="text" || $fieldType=="varchar") return "''";
    }
    $c2db->spParams[$fieldName] = $fieldValue;
    return ":".$fieldName;
}
function c2dbInt($fieldValue) {
    if ($fieldValue!="") $fieldValue = (int)@$fieldValue;
    return c2dbStr($fieldValue, "integer");
}
function c2dbBool($boolValue) {
    if ($boolValue=="f") return "false"; // PHP4ではbooleanパラメータは無いので、固定値を渡す。
    if ($boolValue) return "true"; // PHP4ではbooleanパラメータは無いので、固定値を渡す。
    return "false"; // PHP4ではbooleanパラメータは無いので、固定値を渡す。
}
function c2dbTime($fieldValue) {
    return c2dbStr($fieldValue, "timestamp");
}
function c2dbFloat($fieldValue) {
    return c2dbStr($fieldValue, "float");
}
function c2dbGetRows($sql, $getType="GetRows") {
    global $c2db;
    $rsql = c2dbSqlReverse($sql, array_keys($c2db->spParams), array_values($c2db->spParams), $c2db->spTypes);
    if ($sql=="") {
        $emsg = $getType."-->c2dbGetRows()に失敗しました。SQLが指定されていません： ".($statement ? c2dbDebugInfo() : "");
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
    }
    // 接続
    $c2Con = _c2dbGetConnection($sql);
    // Prepare
    $statement = $c2Con->prepare($sql, $c2db->spTypes);
    if (PEAR::isError($statement)) {
        $emsg = c2dbMakeSqlErrorHtml($sql).$getType."-->c2dbGetRows()/mdb->perpare()に失敗しました： ".c2dbDebugInfo($statement);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
    }
    // Execute
    $query_result = $statement->execute($c2db->spParams);
    if (PEAR::isError($query_result)) {
        $emsg = c2dbMakeSqlErrorHtml($sql).$getType."-->c2dbGetRows()/mdb->execute()に失敗しました： ".c2dbDebugInfo($query_result);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
    }
    if ($getType=="GetOne") $fetch_result = $query_result->fetchOne(); // 先頭フィールド取得
    if ($getType=="GetTopRow") $fetch_result = $query_result->fetchRow(MDB2_FETCHMODE_ASSOC); // 先頭行取得
    if ($getType=="GetRows") $fetch_result = $query_result->fetchAll(MDB2_FETCHMODE_ASSOC); // 全行取得
    if ($getType!="GetResult") $query_result->free();
    $statement->free();

    if (!$c2db->isRemainParams) {
        $c2db->spTypes = array();
        $c2db->spParams = array();
    }
    if ($getType!="GetResult") {
        return $fetch_result;
    }
    return $query_result;
}
function c2dbGetOne($sql) {
    return c2dbGetRows($sql, "GetOne");
}
function c2dbGetTopRow($sql) {
    return c2dbGetRows($sql, "GetTopRow");
}
function c2dbGetResult($sql) {
    return c2dbGetRows($sql, "GetResult");
}
function c2dbGetNext($cursor) {
    $row = $cursor->fetchRow(MDB2_FETCHMODE_ASSOC);
    if (PEAR::isError($row)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."c2dbGetNext()/mdb->fetchRow()に失敗しました： ".c2dbDebugInfo($row);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
    }
    if (!$row) {
        if ($cursor) $cursor->free();
    }
    return $row;
}
function c2dbExec($sql) {
    global $c2db;
    if ($sql=="") {
        $emsg = "c2dbExec()に失敗しました。SQLが指定されていません： ".c2dbDebugInfo($statement);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
    }

    $c2Con = _c2dbGetConnection($sql); // 接続
    // Prepare
    $statement = $c2Con->prepare($sql, $c2db->spTypes);
    if (PEAR::isError($statement)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."c2dbExec()/mdb->prepare(sql, types)に失敗しました： ".c2dbDebugInfo($statement);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
    }

    // Execute
    $query_result = $statement->execute($c2db->spParams);
    if (PEAR::isError($query_result)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."c2dbExec()/mdb->execute(params)に失敗しました： ".c2dbDebugInfo($query_result);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
    }
    $statement->free();
    if ($c2db->autoExecLogCategoryName) {
        $_sql = c2dbSql($sql);
        $_backtrace = c2GetBacktrace();

        $_statement = $c2Con->prepare("select nextval('c2_exec_sql_log_seq')", array());
        if (PEAR::isError($_statement)) {
            $emsg = "c2dbExec()/mdb->perpare(sql)に失敗しました：c2_exec_sql_log_seqシーケンスが取得できません".c2dbDebugInfo($_statement);
            if ($c2db->errorReturn) return $emsg;
            c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
        }
        // Execute
        $query_result = $_statement->execute(array());
        if (PEAR::isError($query_result)) {
            $emsg = "c2dbExec()/mdb->execute(params)に失敗しました：c2_exec_sql_log_seqシーケンスが取得できません".c2dbDebugInfo($query_result);
            if ($c2db->errorReturn) return $emsg;
            c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
        }
        $nextval = $query_result->fetchOne();
        $s_nextval = "".$nextval;
        // １万行おきに、１００万行以前を消す
        if (strlen($s_nextval)>=7 && substr($s_nextval, -4)=="0000") {
            $del_seq_max = substr($s_nextval, 0, strlen($s_nextval)-4);
            $del_seq_max = ((int)$del_seq_max - 100) . "0000";
            $_statement = $c2Con->prepare("delete from c2_exec_sql_log where seq < ".$del_seq_max, array());
            $query_result = $_statement->execute(array());
        }
        $log_sql =
        " insert into c2_exec_sql_log (".
        "     seq, category_name, sql, backtrace, create_emp_id, create_emp_nm, create_datetime".
        " ) values (".
        "     ".$nextval.", :category_name, :sql, :backtrace, :create_emp_id, :create_emp_nm, current_timestamp".
        " )";
        $_statement = $c2Con->prepare($log_sql, array("text", "text", "text", "text", "text"));
        if (PEAR::isError($_statement)) {
            // コーディングエラー。メッセージは簡単でよい。
            $emsg = "c2dbExec()/mdb->prepare(sql, types)に失敗しました（ 自動SQLログ保存エラー）".c2dbDebugInfo($_statement);
            if ($c2db->errorReturn) return $emsg;
            c2LogErrorAndDie($emsg, $log_sql, __FILE__." ".__LINE__);
        }
        $query_result = $_statement->execute(
            array(
                "category_name"=>$c2db->autoExecLogCategoryName,
                "sql"=>ltrim($_sql),
                "backtrace"=>$_backtrace,
                "create_emp_id"=>C2_LOGIN_EMP_ID,
                "create_emp_nm"=>C2_LOGIN_EMP_LT_NM." ".C2_LOGIN_EMP_FT_NM
            )
        );
        if (PEAR::isError($query_result)) {
            $emsg =
                c2GetBacktrace()."\n\n" . "【SQL】\n".$sql."\n\n".
                "c2dbExec()/mdb->execute(params)に失敗しました：自動SQLログ保存に失敗しました： ".
                c2dbDebugInfo($query_result);
            if ($c2db->errorReturn) return $emsg;
            c2LogErrorAndDie($emsg, $sql, __FILE__." ".__LINE__);
        }
        $_statement->free();
    }
    if (!$c2db->isRemainParams) {
        $c2db->spTypes = array();
        $c2db->spParams = array();
    }
    return true;
}
function c2dbSql($sql){
    global $c2db;
    $keys = array();
    foreach ($c2db->spParams as $k=>$v) $keys[] = $k;
    for ($idx = count($keys)-1; $idx>=0; $idx--) {
        $k = $keys[$idx];
        $type = $c2db->spTypes[$idx];
        if ($type=="int" || $type=="integer") {
            $sql = str_replace(":".$k, (strlen($c2db->spParams[$k]) ? $c2db->spParams[$k] : "null"), $sql);
        }
        else {
            $sql = str_replace(":".$k, "'".$c2db->spParams[$k]."'", $sql);
        }
    }
    return $sql;
}
function c2dbBeginTrans() {
    $c2Con = _c2dbGetConnection($sql); // 接続
    $tran = $c2Con->beginTransaction();
    if(PEAR::isError($tran)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."トランザクション開始エラー： ".c2dbDebugInfo($tran);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, "", __FILE__." ".__LINE__);
    }
}
function c2dbCommit() {
    $c2Con = _c2dbGetConnection($sql); // 接続
    $tran = $c2Con->commit();
    if(PEAR::isError($tran)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."トランザクションコミットエラー： ".c2dbDebugInfo($tran);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, "", __FILE__." ".__LINE__);
    }
}
function c2dbRollback() {
    $c2Con = _c2dbGetConnection($sql); // 接続
    $tran = $c2Con->rollback();
    if(PEAR::isError($tran)) {
        $emsg = c2dbMakeSqlErrorHtml($sql)."トランザクションロールバックエラー： ".c2dbDebugInfo($tran);
        if ($c2db->errorReturn) return $emsg;
        c2LogErrorAndDie($emsg, "", __FILE__." ".__LINE__);
    }
}
function c2dbSqlReverse($sql, $columns = NULL, $values, $spTypes) {
    for ($i = 0, $size = sizeof($columns); $i < $size;  $i++) {
        $sql = str_replace(":".$columns[$i], "".var_export($values[$i], TRUE)."", $sql);
    }
    return $sql;
}

function c2dbMakeSqlErrorHtml($sql) {
    return c2GetBacktrace()."\n\n" . ($sql ? "【SQL】\n".c2dbSql($sql)."\n\n" : "");
}














//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// CORE 2 Utils 省略版
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――

//--------------------------------------------------------------------
// JavaScript構文としてエスケープ
// ダブルクォート、改行文字、バックスラッシュの３つでＯＫ
//--------------------------------------------------------------------
function c2js($s) {
    return mb_ereg_replace('"', '\"', mb_ereg_replace("\r", '', mb_ereg_replace("\n", "\\n", mb_ereg_replace("\\\\", '\\\\', $s))));
}


function makeYmdJp($y, $m, $d) {
	$y = ltrim($y, "0");
	$m = ltrim($m, "0");
	$d = ltrim($d, "0");
    return $y. ($y!="" ? "年":"") . $m . ($m!="" ? "月":"") . $d . ($d!="" ? "日":"");
}
function revisionYmd($ymd) {
	$ary = explode("/", $ymd);
	$y = ltrim($ary[0], "0");
	$m = ltrim($ary[1], "0");
	$d = ltrim($ary[2], "0");
    return $y. ($y!="" ? "/":"") . $m . ($m!="" ? "/":"") . $d . ($d!="" ? "/":"");
}

function slashYmdHms($v) {
	if (strlen($v)!=14) return $v;
	return $v[0].$v[1].$v[2].$v[3]."/".$v[4].$v[5]."/".$v[6].$v[7]." ".$v[8].$v[9].":".$v[10].$v[11].":".$v[12].$v[13];
}
function slashYmd($v) {
	if (strlen($v)!=8) return $v;
	return $v[0].$v[1].$v[2].$v[3]."/".$v[4].$v[5]."/".$v[6].$v[7];
}
function getSougouRankTag($rank, $uprank, $msg, $msg2="") {
	if ($msg) $msg .= "\n";
	$html = '<div style="padding-left:4px; line-height:13px; padding-top:1px; ';
	if ($uprank=="up")   return $html.'no-repeat 0 1px" title="'.$msg.'前回よりランクUPしています">'.$rank.'</div>';
	if ($uprank=="down") return $html.'no-repeat 0 10px" title="'.$msg.'前回よりランクDOWNしています">'.$rank.'</div>';
	if ($uprank=="even") return $html.'no-repeat 0 5px" title="'.$msg.'前回と比較して、ランクに変更ありません">'.$rank.'</div>';
	if ($uprank=="odd") return $html.'no-repeat 0 6px" title="'.$msg.'前回ランクの記号が、この回には存在しないため、比較できません'.$msg2.'">'.$rank.'</div>';
	return '<div style="padding-left:4px; line-height:26px;" title="'.$msg.'比較できる前回データは、画面表示内にありません">'.$rank.'</div>';
}

//--------------------------------------------------------------------
// JavaScript構文としてエスケープ
// ダブルクォート、改行文字、バックスラッシュ、シングルクォートの４つに加えてシングルクォートも
//--------------------------------------------------------------------
function c2jsS($s) {
    return mb_ereg_replace("'", "\'", c2js($s));
}

// 職員名からスペースを除去しつつ、全角カナ、半角数値、半角大文字アルファへ変換
//function eliminateSpace($s) {
//  return strtoupper(mb_convert_kana(str_replace("　", "", str_replace(" ", "", $s)), "KCVa"));
//}

//--------------------------------------------------------------------
// 連想配列をJSONへ変換
// 外部LIBは細かい変換仕様が不明瞭かつ口にあわないし、
// バージョンUPによって挙動が変わる可能性や懸念は排除したい。あまり利用したくない。
// やりたいことは些細なことなので、自作定義。
//--------------------------------------------------------------------
function c2ToJson($var, $is_recursive=0) {
    if (gettype($var) == 'array') {
        // 連想配列の場合
        if (is_array($var) && count($var) && (array_keys($var) !== range(0, sizeof($var) - 1))) {
            $properties = array();
            foreach ($var as $k => $v) {
                $quot = 0;
                if ($k=="") $quot = 1;
                else if (preg_match("/[0-9]/", $k[0])) $quot = 1; // 先頭が数値ならクォート
                else if (strpos($k, "@")!==FALSE) $quot = 1;
                $properties[] = ($quot?'"'.$k.'"':$k).":".c2ToJson($v, 1);
            }
            return '{' . join(',', $properties) . '}';
        }
        // 通常の配列の場合
        $properties = array();
        foreach ($var as $v) $properties[] = c2ToJson($v, 1);

        return '[' . join(',', $properties) . ']';
    }
    // それ以外なら、文字列として処理
    if (!$is_recursive && $var=="") return "{}";
    return '"' . c2js($var) . '"';
}


function c2OutputCsvHeader($fileName, $content_length = 0) {
    header("Content-Disposition: attachment; filename=".$fileName);
    header("Content-Type: application/octet-stream; name=".$fileName);
    if ($content_length) header("Content-Length: " . $content_length);
}


function c2GetBacktrace($separator = "\n") {
    if (!function_exists("debug_backtrace")) return "";
    $ary = debug_backtrace();
    $out = array();
    $prev = "";
    for ($idx = count($ary)-1; $idx>=0; $idx--) {
        if ($ary[$idx]["function"]=="c2GetBacktrace") continue;
        if ($ary[$idx]["function"]=="c2dbMakeSqlErrorHtml") continue;
        array_unshift($out,
        "その前【".basename(dirname($ary[$idx]["file"]))."/".basename($ary[$idx]["file"])."】 --- "
        .$prev.$ary[$idx]["line"]."行目で ".$ary[$idx]["function"]."() を実行"
        );
        $prev = "  ".$ary[$idx]["function"]."() 関数";
        if ($ary[$idx]["function"]=="c2dbExec") break;
    }
    return implode($separator, $out);
}

//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
// エラー処理関連
//―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――
function showErrorViewAndDie($msg) {
    showErrorView($msg);
    die;
}
function showErrorView($msg) {
    // 画面にエラー即出し終了。エラー画面には遷移しない。
    echoStaticHtml();
    echo '<body>';
    echo '<div>';
    echo $msg;
    echo '</div>';
    echo '<div><a href="javascript:void(0);" onclick="history.back(); return false;">戻る</a></div>';
    echo '</body></html>';
}
function c2SessionErrorAndDie() {
	if ($_REQUEST["try_download"] || $_REQUEST["try_download_xls1"] || $_REQUEST["try_download_xls2"]) {
		alertDownloadErrorAndDie("長時間利用されませんでしたのでタイムアウトしました。再度ログインしてください。");
	}
    if (defined("C2_AJAX_ACTION_NAME")) {
        if ($_REQUEST[C2_AJAX_ACTION_NAME]) { echo "SESSION_EXPIRED"; die; }
	}
	header("Location: login.html");
	die;
}
function c2AuthErrorAndDie() {
	if ($_REQUEST["try_download"] || $_REQUEST["try_download_xls1"] || $_REQUEST["try_download_xls2"]) {
		alertDownloadErrorAndDie("アクセス権限がありません。");
	}
    if (defined("C2_AJAX_ACTION_NAME")) {
        if ($_REQUEST[C2_AJAX_ACTION_NAME]) { echo "NO_AUTH"; die; }
	}
	header("Location: ./");
	die;
}

function alertDownloadErrorAndDie($msg) {
	echoStaticHtml();
	echo '<script type="text/javascript">';
	echo 'function initAlert() { alert("'.$msg.'"); }';
	echo '</script>';
	echo '</head><body onload="initAlert()"></body></html>';
	die;
}
function c2LogErrorAndDie($errmsg, $sql, $err_place) {
    $fname = c2GetCallerFileName("");
    $errorDate = date("D M j G:i:s T Y");  // 例  Sat Mar 10 15:16:08 MST 2001
    error_log($errorDate ."--> ".$fname.":".$errmsg.($sql?"\n":"").$sql. "\n",3,"./log/error.log");

    // メールの送信
    mb_language("Japanese");
    mb_internal_encoding("UTF-8");
    $body = "".
    $errorDate."\n".
    $err_place."\n".
    $errmsg."\n".
    "【セッション値】\n".print_r($_SESSION, true).
    "\n【リクエスト値】\n".print_r($_REQUEST,true);
    if(mb_send_mail(ERR_MAIL_TO, "【人事評価Navigator】エラー発生", $body ,"From:".ERR_MAIL_FR)) $res =  "※エラーはサイト管理者に通知されました。";
    else $res =  "※エラーの発生を管理者にお知らせください。";
    $errmsg .= "\n".$res;
    // AJAXモードでのエラーの場合
    if (defined("C2_AJAX_ACTION_NAME")) {
        if ($_REQUEST[C2_AJAX_ACTION_NAME]) {
            echo "システムエラーが発生しました。申し訳ございません。\n\n".$res;
            die;
//            echo "【致命的エラー発生】\n";
//            echo $err_place."\n".$errmsg;
//            die; //アプリ終了
        }
    }

    $errormsg = "";
    if ($GLOBALS["C2_ERROR_DB_CONNECTION"]) $errormsg = "データベース接続に失敗しました。";
    else {
        $strong = '<span style="color:red; font-weight:bold">';
        $sqle = '[Native message: ERROR:';
        $sqle_pos = strpos($errmsg, $sqle);
        if ($sqle_pos > 0) {
            $errmsg = str_replace($sqle, $sqle.'</span>'.$strong, $errmsg);
        }
        $errmsg = str_replace("【", '【'.$strong, $errmsg);
        $errmsg = str_replace("】", '</span>】', $errmsg);
        $errmsg = str_replace("行目で", '行目で'.$strong, $errmsg);
        $errmsg = str_replace("を実行", '</span>を実行', $errmsg);
        $errormsg = "<br><br>【致命的エラー発生】<br>".$err_place."<br><span>".str_replace("\n", "</span><br><span>",$errmsg)."</span>";
    }

    showErrorView("システムエラーが発生しました。申し訳ございません。<br><br>".$res);
//    showErrorView($errormsg);

    die;
}

// ２個前の実行ファイルのファイル名を返す
function c2GetCallerFileName($fname) {
    if (function_exists("debug_backtrace")) {
        $ary = debug_backtrace();
        $ret = basename($ary[1]["file"]);
        if ($ret) return $ret;
    }
    if ($fname) return $fname;
    return $fname;
}

function c2Crlf2Br($s) {
    return str_replace("\n", "\n<br>", $s);
}

// 文字列をCSV取得
// 行区切りはShift_JISにつき復帰改行CRLFなのだが、
// 改行LFだけで判定し、分割された文字列から復帰CRをトリムするほうが安全だろう。
function c2GetCsv($cont, $delim = "\n", $is_line = false) {
    // 先頭にUTF8-BOMがあれば除去する
    if (!$is_line) {
        if (substr($cont, 0, 3)==pack('C*',0xEF,0xBB,0xBF)) $cont = substr($cont, 3);
    }
    $out = array();
    $ttl_len = strlen($cont);
    $offset = 0; // 文字列シーク位置カーソル
    $mod = 0; // Wクォート偶数奇数判定用
    $len = 0; // 文字列長
    $dlen = strlen($delim);
    $line_offset = 0;
    $sepa = "";
    for (;;) {
        $pos = strpos($cont, $delim, $line_offset+$offset); // デリミタの位置。文字列先頭がデリミタならゼロ
        if ($pos===FALSE && strlen($cont) - $line_offset - $offset > 0) $pos = strlen($cont); // デリミタ無しなら、終端の「次の文字」にデリミタがある、と仮定
        $slen = $pos - $line_offset - $offset; // 区切り文字を含んでいるかもしれない区間文字列長。
        if ($pos>0) {
            $part = substr($cont, $line_offset + $offset, $slen); // 区間文字列。区切り文字を含んでいるかもしれない。
            $quot = $mod + substr_count($part, '"'); // 区間文字列内のWクオートの数＋$mod。前回ループで奇数なら$modは1である。
            $mod = $quot % 2;// Wクォート数が偶数か奇数か。奇数なら改行コードは区切り文字でなくてデータである。
        }
        // 末尾未到達で、かつWクォート奇数ならオフセットを進める
        if ($mod==1 && ($line_offset+$len+$slen+$dlen < $ttl_len)) { $offset += $slen+$dlen; $len += $slen+$dlen; continue; }
        if ($is_line) {
            $out[]= _c2GetCsvEliminateWQuort(substr($cont, $line_offset+$offset, $len+$slen), $delim); // 列取得モードなら出力にそのまま追加
        } else {
            $line = rtrim(rtrim(substr($cont, $line_offset, $len+$slen+$dlen), "\r"), $delim); // 行データ
            // 項目区切り文字が決定していない場合（初回探索）
            if (!strlen($sepa)) {
                if (strpos($line, "\t")!==FALSE) $sepa = "\t"; // 本システムはCSVデータにタブを含まないので、もしタブがあればタブ区切りとみなす。
                else $sepa = ","; // そうでなければ、カンマ区切りであるとする。
            }
            $out[]= c2GetCsv($line, $sepa, true); // 行取得モードなら列を取得
        }
        $line_offset += $len + $slen+$dlen; // 次のデリミタ以降から開始するように
        if ($line_offset >= $ttl_len) break; // 最後に達したので終了
        $len = 0;
        $offset = 0;
        $mod = 0;
    }
    return $out;
}

function _c2GetCsvEliminateWQuort($str, $delim) {
    if (substr($str, -1)=="\r") $str = substr($str, 0, strlen($str)-1);
    $is_quorted = 0;
    if (substr($str, 0, 1)=='"') { $is_quorted++; $str = substr($str, 1); }
    if (substr($str, -1)=='"') { $is_quorted++; $str = substr($str, 0, strlen($str)-1); }
    // タブ区切りTSVである場合。文字列がダブルクオートされていない場合は、その中に含まれるWクォートはそのまま文字である。
    if ($delim=="\t" && $is_quorted!=2) return $str;
    // 通常はこちら。CSVか、TSVでもダブルクォートで囲まれている場合。その中に含まれるWクォートは、２個セットで１個である。
    return str_replace('""', '"', $str);
}



//******************************************************************************
// HTMLエスケープ、UTF-8につき簡単でよい
//******************************************************************************
function hh($v) {
    if (is_array($v)) return "htmlspecialcharsへの引数が不正：".c2GetBacktrace();
    return htmlspecialchars($v);
}

//******************************************************************************
// 垂直タブ除去 vertical tab elimination
//******************************************************************************
function ve($s) {
    return preg_replace('/[\x0b]/', '', $s);
}

//******************************************************************************
//
//******************************************************************************
function c2NumberFormat($v) {
    if (!$v) return ""; // ゼロはカラで返す
    return number_format($v);
}



//******************************************************************************
//
//******************************************************************************
function echoStaticHtml() {
    $out = array();
    $out[]= '<!DOCTYPE html>';
    $out[]= '<html>';
    $out[]= '<head>';
    $out[]= '<meta http-equiv="X-UA-Compatible" content="IE=Edge" />';
    $out[]= '<meta http-equiv="Expires" content="0" />';
    $out[]= '<meta http-equiv="Cache-Control" content="no-cache" />';
    $out[]= '<meta http-equiv="Pragma" content="no-cache" />';
    $out[]= '<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />';
    $out[]= '<meta name="robots" content="noindex,nofollow" />';
    $out[]= '<title>人事評価Navigator</title>';
    $out[]= '<link rel="stylesheet" type="text/css" href="css.css?v=2" />';
    $out[]= '<link rel="stylesheet" type="text/css" href="print.css?v=2" media="print" />';
    $out[]= '<link rel="stylesheet" type="text/css" href="./lib/css/jquery-ui.min.css" />';

    $out[]= '<script type="text/javascript" src="jquery-1.11.2.min.js"></script>';
    $out[]= '<script type="text/javascript" src="./lib/js/underscore-min.js"></script>';
    $out[]= '<script type="text/javascript" src="./lib/js/jquery-ui.min.js"></script>';
    $out[]= '<script type="text/javascript" src="js.js?v=2"></script>';
    echo implode("\r\n", $out);
}

function echoMenuLinks($is_admin_view){
    $revnm = @$_SESSION["REVISION_ROW"]["revnm"];
    $revnm_mbo = @$_SESSION["REVISION_MBO_ROW"]["revnm"];
    $hyo_step = (int)$_SESSION["REVISION_ROW"]["hyo_step"];
    $fname = basename($_SERVER["PHP_SELF"]);
    $bold = array($fname => ' class="menu_current"');
    $out = array();
    $loginRow = $_SESSION["EMPMST_ROW"];
    $clientRow = $_SESSION["CLIENT_ROW"];

    //======================================================
    // ダウンロード用隠しIFRAME
    //======================================================
    $out[]= '<iframe name="dframe" id="dframe" style="width:100px height:100px; visibility:hidden; position:absolute; left:-200px; top:-200px"';
    $out[]= ' tabIndex="-1"></iframe>';

    $out[]= '<div id="fixed_header">';
    $out[]= '<div style="height:50px; line-height:50px; color:#ffffff; position:relative">';

    //======================================================
    // サイトロゴ・サイトタイトル
    //======================================================
    //$out[]= '<div id="div_cap"></div>';
    $out[]= '<div id="div_logo"></div>';

    if ($fname=="dev.html") {
        $out[]= '<div style="position:absolute; font-size:22px; left:400px; top:6px; letter-spacing:3px; color:#ddffff">&lt;dev.html&gt;</div>';
    }
    if (($fname=="jissi.html" || $fname=="hyokaEmpsMbo.html") && $_REQUEST["arch_load"]) {
        $out[]= '</div>';
        echo implode("\n", $out);
        echo '</div>';
        echo '<div id="margin_header" style="height:60px"></div>';
        return;
    }

    //======================================================
    // 管理画面／サイトトップ切替
    //======================================================
    if ($_SESSION["EMPMST_ROW"]["is_admin"] && $fname!="dev.html") {
        $admin_first_page = "revisions.html";
        if ($_SESSION["SUPER_CID"]) $admin_first_page = "clients.html";
        if ($is_admin_view) {
            $out[]= '<a id="to_top" class="head_btn" href="index.html"><div style="letter-spacing:0; padding-left:40px; padding-top:8px; font-size:12px">サイトトップ</div></a>';
            $out[]= '<a id="to_kanri_on" class="head_btn" href="'.$admin_first_page.'" title="管理画面へは、サイト管理者のみ遷移できます。"><div>管理画面</div></a>';
        }
        else {
            $out[]= '<a id="to_kanri" class="head_btn" href="'.$admin_first_page.'" title="管理画面へは、サイト管理者のみ遷移できます。"><div>管理画面</div></a>';
            $out[]= '<a id="to_top_on" class="head_btn" href="index.html"><div style="letter-spacing:0; padding-left:40px; padding-top:8px; font-size:12px">サイトトップ</div></a>';
        }
    }


    $out[]= '<table cellspacing="0" cellpadding="0" class="width100" style="font-size:12px"><tr>';
    $out[]= '<td style="padding-left:600px; width:auto"></td>';


    //======================================================
    // 代替ログイン
    //======================================================
    if ($_SESSION["SUPER_CID"] && $_SESSION["EMPMST_ROW"]["is_admin"]) {
        $out[]= '<td style="padding:5px 4px 0 4px; width:10%; text-align:center;">';
        $out[]= '<div style="color:#c1ccea; font-size:11px; padding-bottom:2px"><nobr>＊ 代替ログイン先 ＊</nobr></div>';
        $out[]= '<a id="a_client_link" href="clients.html"><nobr>'.$clientRow["cnm"]."</nobr></a></td>";
    }

    //======================================================
    // ログイン者・顧客名・権限マーク、ログアウト
    //======================================================
    if ($fname!="login.html") {
        $out[]= '<td style="padding:5px 8px 0 8px; width:10%; text-align:center; color:#eeeeee">';
        $out[]= '<div style="font-size:11px; padding:3px 8px"><nobr>';
        if ($_SESSION["SUPER_CID"]) $out[]= $_SESSION["SUPER_CNM"];
        else $out[]= $clientRow["cnm"];
        $out[]= '</nobr></div>';
        $out[]= '<div style="padding:0 2px"><nobr>';
        $out[]= hh($loginRow["empnm"]);
        if ($_SESSION["EMPMST_ROW"]["is_admin"]) {
            $out[]= '<span title="ログイン者は、サイト管理者です。" style="font-size:11px; background-color:#dddddd; border:1px solid #dddddd; color:#3a5baf">管理</span>';
        }
        //$out[]= '<span title="ログイン者は、この評価期間における最終判定者です。" style="font-size:11px; background-color:#dddddd; border:1px solid #dddddd; color:#3a5baf;';
        //if (!@$_SESSION["CAST_ROW"]["is_hyomaster"] && !$_SESSION["IS_LOGIN_HYOMASTER"]) $out[]= ' display:none;';
        //$out[]= '">総判</span>';

        $out[]= '</nobr></div>';
        $out[]= '</td>';
        $out[]= '<td id="td_logout"><div onclick="location.href=\'login.html\'">ログアウト</div></td>';
    }
    $out[]= '</tr></table>';
    $out[]= '</div>';



    //======================================================
    // メインメニュー
    //======================================================
    if ($fname!="login.html" && $fname!="dev.html") {
        //======================================================================
        // 管理機能
        //======================================================================
        if ($is_admin_view) {
            $out[]= '<div style="border-top:1px solid #777777"><table cellspacing="0" cellpadding="0" id="main_link"><tr>';
            if ($_SESSION["SUPER_CID"] && $_SESSION["EMPMST_ROW"]["is_admin"]) {
                $out[]= '<td onclick="location.href=\'clients.html\'"'.$bold["clients.html"].'><nobr>クライアント</nobr></td>';
            }
            $out[]= '<td onclick="location.href=\'revisionsTtl.html\'"'.$bold["revisionsTtl.html"].$bold["castsTtl.html"].$bold["castTtlImp.html"].'><nobr>総合判定設定</nobr></td>';
            $out[]= '<td onclick="location.href=\'revisions.html\'"'.$bold["revisions.html"].$bold["scores.html"].$bold["casts.html"].$bold["castimp.html"].'><nobr>行動評価設定</nobr></td>';
            $out[]= '<td onclick="location.href=\'revisionsMbo.html\'"'.$bold["revisionsMbo.html"].$bold["formMbo.html"].$bold["castsMbo.html"].$bold["castmboimp.html"].'><nobr>目標達成度評価設定</nobr></td>';
            $out[]= '<td onclick="location.href=\'emps.html\'"'.$bold["emps.html"].$bold["empimp.html"].'><nobr>職員設定</nobr></td>';
            $out[]= '<td onclick="location.href=\'options.html\'"'.$bold["options.html"].'><nobr>その他設定</nobr></td>';
            //    $out[]= '<td onclick="location.href=\'kihonkyu.html\'"'.$bold["kihonkyu.html"].'><nobr>基本給表</nobr></td>';
            $out[]= '<td style="width:auto" class="no_button"></td>';
            $out[]= '</tr></table></div>';
        }
        //======================================================================
        // ユーザ機能
        //======================================================================
        else {
            if ($bold["jissi.html"] && $_REQUEST["hstep"]) $bold = array("hstep".$_REQUEST["hstep"] => ' class="menu_current"');
            $out[]= '<table cellspacing="0" cellpadding="0"><tr>';
            if ($fname!="index.html") {
                if ($revnm && ( $fname=="jikohyoka.html" || $fname=="hyouka.html" || $fname=="sougou.html" || $fname=="jissi.html"  || $fname=="sougou.html" )) {
                  $out[]= '<td id="revnm_panel2_title"><div><nobr>評価期間</nobr></div></td>';
                  $out[]= '<td id="revnm_panel2"><div>'.hh($revnm).' [行動評価]</div></td>';
                }
                if ($revnm_mbo && ($fname=="hyokaEmpsMbo.html" || $fname=="hyokaMbo.html" || $fname=="jikohyokaMbo.html" || $fname=="mokuhyoEntry.html" || $fname=="mokuhyoApprove.html" || $fname=="mokuhyoEmps.html" || $fname=="saisyuMbo.html")) {
                  $out[]= '<td id="revnm_panel2_title"><div><nobr>評価期間</nobr></div></td>';
                  $out[]= '<td id="revnm_panel2"><div>'.hh($revnm_mbo).' [目標達成度評価]</div></td>';
                }
                $out[]= '<td style="padding-top:4px; padding-left:4px"><button class="btn130 btn_index" onclick="location.href=\'index.html\'"'.BTN_FOCUS_BLUR.'><div>トップページへ</div></button></td>';
                $out[]= '<td style="padding:4px 0 0 4px"><button class="btn130 btn_cancel2" id="head_jissi_cancel" style="display:none" onclick="closeEditor()"'.BTN_FOCUS_BLUR.'><div>一覧へ戻る</div></button></td>';
            }
            $out[]= '</tr></table>';
        }
    }
    $out[]= '</div><!-- // fixed_header -->';
    $out[]= '<div id="margin_header"></div>';
    echo implode("\n", $out);
}








//**********************************************************************************************************************
//
//**********************************************************************************************************************

set_include_path(get_include_path() . PATH_SEPARATOR . dirname(dirname(__FILE__)) . '/PEAR');

// セッション開始。仮に同じ端末でディレクトリ違いな場合や、ドメインが変わった場合も再ログインのこと。
session_start();
if ($_SESSION["SERVER_NAME"]!=$_SERVER["SERVER_NAME"]) {
    $_SESSION["SERVER_NAME"] = $_SERVER["SERVER_NAME"];
    header("Location: login.html?retype=1"); // retypeはダミー目印。
    die;
}
if ($_SESSION["THIS_FILE"]!=__FILE__) {
    $_SESSION["THIS_FILE"] = __FILE__;
    header("Location: login.html?retype=2"); // retypeはダミー目印。
    die;
}

require_once('PEAR.php');
require_once('MDB2.php'); // DB接続定義。PEAR利用。

// ログイン済CIDがあるのに、CIDが抹消されている場合はログアウト。
if ($_SESSION["CID"]) {
    if (!c2dbGetOne("select relname from pg_class where relkind = 'r' and relname = ".c2dbStr($_SESSION["CID"]."_hnv_revisions"))) {
        if ($_SESSION["SUPER_CID"]) {
            $_SESSION["CID"] = $_SESSION["SUPER_CID"];
        } else {
            header("Location: login.html?retype=3"); // retypeはダミー目印。
            die;
        }
    }
}

define("C2_AJAX_ACTION_NAME", "ajax_action");
$req = $_REQUEST; // 変数名が長いから短く

define("BTN_FOCUS_BLUR", ' onfocus="$(this).addClass(\'focussing\')" onblur="$(this).removeClass(\'focussing\')"');
define("BTN_FOCUS_BLUR_JS", str_replace("'", "\'", BTN_FOCUS_BLUR));
